#import tensorflow as tf
from keras.layers.normalization import BatchNormalization
from keras.layers import Dropout,LeakyReLU,ReLU,Conv1D,Dense,Flatten,Input,MaxPooling1D,Concatenate,Reshape,LSTM,Embedding,Bidirectional
from keras.initializers import he_normal
from keras.models import Model,Sequential
from keras.regularizers import l2
#import resNet as rn
from keras import regularizers
#from Mish import Mish
from keras import backend as K

def build_model_PostProcessing(inputSize,arch):

	if arch == 1:

		inputs = Input(shape=(inputSize,1))

		x = Conv1D(kernel_size=3,filters=32,strides=1,padding='same',use_bias=True)(inputs)
		x = LeakyReLU()(x)
		x1 = Dropout(rate=0.5)(x)
		#x = BatchNormalization()(x)
		
		x = Conv1D(kernel_size=3,filters=32,strides=1,padding='same',use_bias=True)(x1)
		x = BatchNormalization()(x)
		x = LeakyReLU()(x)

		x = Concatenate(axis=2)([x,x1])

		x = Conv1D(kernel_size=3,filters=64,strides=1,padding='same',use_bias=True)(x)
		x = LeakyReLU()(x)
		x = Dropout(rate=0.5)(x)
		
		x = MaxPooling1D(pool_size=2,strides=2,padding='same')(x)

		x = Conv1D(kernel_size=3,filters=64,strides=1,padding='valid',use_bias=True)(x)
		x = BatchNormalization()(x)
		x = LeakyReLU()(x)
		x2 = Dropout(rate=0.5)(x)

		x = Conv1D(kernel_size=3,filters=64,strides=1,padding='same',use_bias=True)(x2)
		x = BatchNormalization()(x)
		x = LeakyReLU()(x)

		x = Flatten()(x)

		x = Dense(512)(x)
		x = BatchNormalization()(x)
		x = LeakyReLU()(x)
		x = Dropout(rate=0.5)(x)

		x = Dense(256)(x)

		outputs = Dense(1,activation='sigmoid')(x)
		
		model = Model(inputs,outputs)
	elif arch == 2:
		inputs = Input(shape=(inputSize,1))
		
		x = Conv1D(kernel_size=3,filters=16)(inputs)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=16)(x)
		x = ReLU()(x)
		x = MaxPooling1D()(x)
		
		x = Conv1D(kernel_size=3,filters=32)(x)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=32)(x)
		x = ReLU()(x)
		x = MaxPooling1D()(x)

		x = Conv1D(kernel_size=3,filters=64)(x)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=64)(x)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=64)(x)
		x = ReLU()(x)
		x = MaxPooling1D()(x)

		x = Conv1D(kernel_size=3,filters=128)(x)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=128)(x)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=128)(x)
		x = ReLU()(x)
		x = MaxPooling1D()(x)
		
		x = Flatten()(x)

		x = Dense(1024)(x)
		x = ReLU()(x)
		x = Dense(1024)(x)
		x = ReLU()(x)
		outputs = Dense(1,activation='sigmoid')(x)
		
		model = Model(inputs,outputs)

	elif arch == 3:
		inputs = Input(shape=(inputSize,1))
		
		x = Conv1D(kernel_size=3,filters=16)(inputs)
		x = ReLU()(x)
		x = MaxPooling1D()(x)
		
		#x = Conv1D(kernel_size=3,filters=32)(x)
		#x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=32)(x)
		x = ReLU()(x)
		x = MaxPooling1D()(x)

		x = Conv1D(kernel_size=3,filters=64)(x)
		x = ReLU()(x)
		#x = Conv1D(kernel_size=3,filters=64)(x)
		#x = ReLU()(x)
		#x = Conv1D(kernel_size=3,filters=64)(x)
		#x = ReLU()(x)
		#x = MaxPooling1D()(x)

		x = Conv1D(kernel_size=3,filters=128)(x)
		x = ReLU()(x)
		#x = Conv1D(kernel_size=3,filters=128)(x)
		#x = ReLU()(x)
		#x = Conv1D(kernel_size=3,filters=128)(x)
		#x = ReLU()(x)
		#x = MaxPooling1D()(x)
		
		x = Flatten()(x)

		x = Dense(1024)(x)
		x = ReLU()(x)
		#x = Dense(1024)(x)
		#x = ReLU()(x)i
		outputs = Dense(1,activation='sigmoid')(x)
		
		model = Model(inputs,outputs)
	elif arch == 4:

		inputs = Input(shape=(inputSize,1))
		
		#x = Conv1D(kernel_size=12,filters=20)(inputs)
		#x = LeakyReLU()(x)

		#x = Conv1D(kernel_size=12,filters=20)(x)
		#x = LeakyReLU()(x)

		#x = Conv1D(kernel_size=12,filters=20)(x)
		#x = LeakyReLU()(x)

		x = Conv1D(kernel_size=5,filters=15)(inputs)
		x = LeakyReLU()(x)

		#x = Conv1D(kernel_size=9,filters=10)(x)
		#x = LeakyReLU()(x)

		#x = Conv1D(kernel_size=12,filters=7)(x)
		#x = LeakyReLU()(x)
		

		x = Flatten()(x)
		x = Dense(4096)(x)
		x = ReLU()(x)
		x = Dense(2048)(x)
		x = ReLU()(x)
		x = Dense(1024)(x)
		x = ReLU()(x)
		x = Dense(512)(x)
		x = ReLU()(x)
		#x = Dense(256,kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4),
		#	bias_regularizer=regularizers.l2(1e-4),
		#	activity_regularizer=regularizers.l2(1e-5))(x)
		#x = ReLU()(x)
		outputs = Dense(2,activation='linear')(x)
		
		model = Model(inputs,outputs)

	if arch == 5:

		inputs = Input(shape=(inputSize,1))
			
		x = Conv1D(kernel_size=12,filters=20,activation=mish)(inputs)

		x = Conv1D(kernel_size=12,filters=20,activation=mish)(x)

		x = Conv1D(kernel_size=12,filters=20,activation=mish)(x)

		x = Conv1D(kernel_size=9,filters=15,activation=mish)(x)

		x = Conv1D(kernel_size=9,filters=10,activation=mish)(x)

		x = Conv1D(kernel_size=9,filters=7,activation=mish)(x)
		
		x = Flatten()(x)
		x = Dense(512,activation=mish)(x)
		
		outputs = Dense(3,activation='linear')(x)
		model = Model(inputs,outputs)


	return model


def build_architect_test(window_size,features=1,arch=1):

	if arch == 1:


		inputs = Input(shape=(window_size,1))
			
		x = Conv1D(kernel_size=3,filters=16)(inputs)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=16)(x)
		x = ReLU()(x)
		x = MaxPooling1D()(x)

		x = Conv1D(kernel_size=3,filters=32)(x)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=32)(x)
		x = ReLU()(x)
		x = MaxPooling1D()(x)

		x = Conv1D(kernel_size=3,filters=64)(x)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=64)(x)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=64)(x)
		x = ReLU()(x)
		x = MaxPooling1D()(x)

		x = Conv1D(kernel_size=3,filters=128)(x)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=128)(x)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=128)(x)
		x = ReLU()(x)
		x = MaxPooling1D()(x)

		x = Flatten()(x)

		x = Dense(1024)(x)
		x = ReLU()(x)
		x = Dense(1024)(x)
		x = ReLU()(x)
		outputs = Dense(1,activation='linear')(x)
		
		model = Model(inputs,outputs)
	if arch ==2:
		
		inputs = Input(shape=(window_size,1))

		x = Conv1D(kernel_size=3,filters=32,strides=1,padding='same',use_bias=True)(inputs)
		x = LeakyReLU()(x)
		x1 = Dropout(rate=0.5)(x)
		#x = BatchNormalization()(x)
		
		x = Conv1D(kernel_size=3,filters=32,strides=1,padding='same',use_bias=True)(x1)
		x = BatchNormalization()(x)
		x = LeakyReLU()(x)

		x = Concatenate(axis=2)([x,x1])

		x = Conv1D(kernel_size=3,filters=64,strides=1,padding='same',use_bias=True)(x)
		x = LeakyReLU()(x)
		x = Dropout(rate=0.5)(x)
		
		x = MaxPooling1D(pool_size=2,strides=2,padding='same')(x)

		x = Conv1D(kernel_size=3,filters=64,strides=1,padding='valid',use_bias=True)(x)
		x = BatchNormalization()(x)
		x = LeakyReLU()(x)
		x2 = Dropout(rate=0.5)(x)

		x = Conv1D(kernel_size=3,filters=64,strides=1,padding='same',use_bias=True)(x2)
		x = BatchNormalization()(x)
		x = LeakyReLU()(x)

		x = Flatten()(x)

		x = Dense(512)(x)
		x = BatchNormalization()(x)
		x = LeakyReLU()(x)
		x = Dropout(rate=0.5)(x)

		x = Dense(256)(x)

		outputs = Dense(1,activation='linear')(x)
		
		model = Model(inputs,outputs)
	if arch == 3:


		inputs = Input(shape=(window_size,1))
			
		x = Conv1D(kernel_size=7,filters=16,strides=1,padding='same',use_bias=True)(inputs)
		x = LeakyReLU()(x)
		x = Conv1D(kernel_size=7,filters=16,strides=1,padding='same',use_bias=True)(x)
		x = LeakyReLU()(x)
		x = MaxPooling1D()(x)

		x = Conv1D(kernel_size=5,filters=32,strides=1,padding='same',use_bias=True)(x)
		x = LeakyReLU()(x)
		x = Conv1D(kernel_size=5,filters=32,strides=1,padding='same',use_bias=True)(x)
		x = LeakyReLU()(x)
		x = MaxPooling1D()(x)

		x = Conv1D(kernel_size=5,filters=64,strides=1,padding='same',use_bias=True)(x)
		x = LeakyReLU()(x)
		x = Conv1D(kernel_size=5,filters=64,strides=1,padding='same',use_bias=True)(x)
		x = LeakyReLU()(x)
		x = Conv1D(kernel_size=5,filters=64,strides=1,padding='same',use_bias=True)(x)
		x = LeakyReLU()(x)
		x = MaxPooling1D()(x)

		x = Conv1D(kernel_size=3,filters=128,strides=1,padding='same',use_bias=True)(x)
		x = LeakyReLU()(x)
		x = Conv1D(kernel_size=3,filters=128,strides=1,padding='same',use_bias=True)(x)
		x = LeakyReLU()(x)
		x = Conv1D(kernel_size=3,filters=128,strides=1,padding='same',use_bias=True)(x)
		x = LeakyReLU()(x)
		x = MaxPooling1D()(x)

		x = Flatten()(x)

		x = Dense(1024)(x)
		x = ReLU()(x)
		outputs = Dense(1,activation='linear')(x)
		
		model = Model(inputs,outputs)

	return model

def mish(inputs):
	return inputs * K.tanh(K.softplus(inputs))

def build_architect(window_size,features,arch):

	if arch == 4:

		inputs = Input(shape=(window_size,1))
			
		x = Conv1D(kernel_size=12,filters=20,activation=mish)(inputs)

		x = Conv1D(kernel_size=12,filters=20,activation=mish)(x)

		x = Conv1D(kernel_size=12,filters=20,activation=mish)(x)

		x = Conv1D(kernel_size=9,filters=15,activation=mish)(x)

		x = Conv1D(kernel_size=9,filters=10,activation=mish)(x)

		x = Conv1D(kernel_size=9,filters=7,activation=mish)(x)
		
		x = Flatten()(x)
		x = Dense(512,activation=mish)(x)
		
		outputs = Dense(3,activation='linear')(x)
		model = Model(inputs,outputs)

	if arch == 3:

		inputs = Input(shape=(window_size,1))
			
		x = Conv1D(kernel_size=12,filters=20)(inputs)
		x = LeakyReLU()(x)

		x = Conv1D(kernel_size=12,filters=20)(x)
		x = LeakyReLU()(x)

		x = Conv1D(kernel_size=12,filters=20)(x)
		x = LeakyReLU()(x)

		x = Conv1D(kernel_size=9,filters=15)(x)
		x = LeakyReLU()(x)

		x = Conv1D(kernel_size=9,filters=10)(x)
		x = LeakyReLU()(x)

		x = Conv1D(kernel_size=9,filters=7)(x)
		x = LeakyReLU()(x)
		

		x = Flatten()(x)

		x = Dense(512)(x)
		x = LeakyReLU()(x)
		outputs = Dense(3,activation='linear')(x)
		model = Model(inputs,outputs)

	if arch == 2:
		inputs = Input(shape=(window_size,1))
			
		x = Conv1D(kernel_size=3,filters=16)(inputs)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=16)(x)
		x = ReLU()(x)
		x = MaxPooling1D()(x)

		x = Conv1D(kernel_size=3,filters=32)(x)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=32)(x)
		x = ReLU()(x)
		x = MaxPooling1D()(x)

		x = Conv1D(kernel_size=3,filters=64)(x)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=64)(x)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=64)(x)
		x = ReLU()(x)
		x = MaxPooling1D()(x)

		x = Conv1D(kernel_size=3,filters=128)(x)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=128)(x)
		x = ReLU()(x)
		x = Conv1D(kernel_size=3,filters=128)(x)
		x = ReLU()(x)
		x = MaxPooling1D()(x)

		x = Flatten()(x)

		x = Dense(1024)(x)
		x = ReLU()(x)
		x = Dense(1024)(x)
		x = ReLU()(x)
		outputs = Dense(3,activation='linear')(x)
		model = Model(inputs,outputs)
	if arch == 1:

		inputs = Input(shape=(window_size,features))


		x = Conv1D(kernel_size=3,filters=32,strides=1,padding='same',use_bias=True)(inputs)
		x = LeakyReLU()(x)
		x1 = Dropout(rate=0.5)(x)
		#x = BatchNormalization()(x)
		
		x = Conv1D(kernel_size=3,filters=32,strides=1,padding='same',use_bias=True)(x1)
		x = BatchNormalization()(x)
		x = LeakyReLU()(x)

		x = Conv1D(kernel_size=3,filters=64,strides=1,padding='same',use_bias=True)(x)
		x = LeakyReLU()(x)
		x = Dropout(rate=0.5)(x)
		
		x = MaxPooling1D(pool_size=2,strides=2,padding='same')(x)

		x = Conv1D(kernel_size=3,filters=64,strides=1,padding='valid',use_bias=True)(x)
		x = BatchNormalization()(x)
		x = LeakyReLU()(x)
		x2 = Dropout(rate=0.5)(x)

		x = Conv1D(kernel_size=3,filters=64,strides=1,padding='same',use_bias=True)(x2)
		x = BatchNormalization()(x)
		x = LeakyReLU()(x)

		x = Concatenate(axis=2)([x,x2])

		x = Conv1D(kernel_size=3,filters=64,strides=1,padding='same',use_bias=True)(x)
		x = LeakyReLU()(x)
		x = Dropout(rate=0.5)(x)

		x = MaxPooling1D(pool_size=2,strides=2,padding='same')(x)

		x = Flatten()(x)

		x = Dense(512)(x)
		x = BatchNormalization()(x)
		x = LeakyReLU()(x)
		x = Dropout(rate=0.5)(x)

		x = Dense(256)(x)

		outputs = Dense(3,activation='linear')(x)
		
		model = Model(inputs,outputs)
	return model

def build_transModel(baseModel,window_size):

	x = baseModel.input

	for lay in range(len(baseModel.layers)):
		#if 'concatenate' not in baseModel.layers[lay].name:
		baseModel.layers[lay].trainable=False
		#x = baseModel.layers[lay](x)

	#print(baseModel.summary())
	x = Conv1D(kernel_size=12,filters=20, kernel_initializer = he_normal(),name='Conv1Trainable')(baseModel.layers[-8].output)
	x = BatchNormalization(name='batch1')(x)
	x = LeakyReLU(alpha = 0.3,name='Act1Trainable')(x)
	#x = ReLU()(x)
	
	x = MaxPooling1D(pool_size=2,strides=2,padding='same',name='pool1')(x)

	x = Conv1D(kernel_size=12,filters=20, kernel_initializer = he_normal(),name='Conv2Trainable')(x)
	x2 = LeakyReLU(alpha = 0.3,name='Act2Trainable')(x)
	#x = ReLU()(x)


	x = Conv1D(kernel_size=12,filters=20, kernel_initializer = he_normal(),name='Conv3Trainable')(x2)
	x = BatchNormalization(name='batch2')(x)
	x = LeakyReLU(alpha = 0.3,name='Act3Trainable')(x)
	#x = ReLU()(x)

	x = Concatenate(name='conc1',axis=1)([x,x2])

	x = Conv1D(kernel_size=9,filters=7, kernel_initializer = he_normal(),name='Conv4Trainable')(x)
	x = LeakyReLU(alpha = 0.3,name='Act4Trainable')(x)

	x = Flatten(name='flatten1')(x)
	
	x = Dense(units=512)(x)

	x = Dense(units=3)(x)

	model = Model(inputs=baseModel.input,outputs=x)
	#print(model.summary())
	#exit()
	return model




def main():
	print(build_model_PostProcessing(inputSize=50,arch=3).summary())


if __name__ == '__main__':
	main()






