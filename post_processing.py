import pandas as pd
import matplotlib.pyplot as plt
import build_data as bd
from random import randint,shuffle,random
import nn
from keras.models import load_model
from keras.callbacks import EarlyStopping
import numpy as np
import os
from sklearn.preprocessing import normalize
import copy

os.environ["CUDA_VISIBLE_DEVICES"]="1"
dir_path = str(os.path.dirname(os.path.realpath(__file__)))

def build_windows(energyProfile,label,window_size):

	windows_X = []
	windows_Y = []

	for i in range(0,len(energyProfile),window_size):  # Building windows
				
		if i+window_size > len(energyProfile):
			break
		windows_X.append(energyProfile[i:i+window_size])
		#windows_Y.append([label]*window_size)
		windows_Y.append(label)

	return windows_X,windows_Y

def synthesize_energyProfile_fromActivation(timeSeries,energy_interval):
	energyProfile = []
	for i in range(0,len(timeSeries),energy_interval):  # Building windows
		energyProfile.append(sum(timeSeries[i:i+energy_interval])/energy_interval)

	return energyProfile

def get_activations(appliance,house):
	acts = []
	if appliance == 'kettle':
		a = bd.load_activations(appliance,2)
		acts.extend(a)		
	if appliance == 'dish washer':
		a = bd.load_activations(appliance,2)
		acts.extend(a)
		#a = bd.load_activations(appliance,5)
		#acts.extend(a)
		#a = bd.load_activations(appliance,2)
		#acts.extend(a)
	if appliance == 'washing machine':
		a = bd.load_activations(appliance,2)
		acts.extend(a)
	"""
	for i in range(numActs):	
		with open(dir_path+'/data/acts_'+appliance+'_House'+str(house)+'/'+str(i)+'.txt','r') as arquivo:
		#with open(dir_path+'/data/House'+str(house)+'/acts/acts_'+appliance+'_House'+str(house)+'/'+str(i)+'.txt','r') as arquivo:
			act = []
			for line in arquivo:
				currentPlace = line[:-1]
				act.append(float(currentPlace))
			acts.append(act)
	"""
	return acts


def extract_activations(activations,timeSerie,treshold,tamMin):

	if activations == None:
		activations = []

	aux = [0,0]

	for i in range(len(timeSerie)):
		if timeSerie[i] < treshold and len(aux) > 2:
			aux.append(0)
			aux.append(0)
			if len(aux)>tamMin:
				aux.extend(timeSerie[i:i+800])
				activations.append(aux)
			aux = [0,0]
		elif timeSerie[i] > treshold:
			aux.append(timeSerie[i])
	shuffle(activations)
	return activations


def get_tamMean(activations):


	m = 0
	for i in range(len(activations)):
		m+=len(activations[i])

	return m/len(activations)



def get_positiveSamples(numSamples,appliance,window_size):

	#data = bd.get_data_andMain(appliance=appliance,house=4)
	#activations = extract_activations(None,data[appliance],treshold=10,tamMin=30)

	activations = get_activations(appliance,house=2)
	

	#data = bd.get_data_andMain(appliance=appliance,house=1)
	#activations = extract_activations(activations,data[appliance],treshold=10,tamMin=30)
	
	lMean = int(get_tamMean(activations))
	#lMean = 1000
	print("Len mean:",lMean)
	exit()
	posSamples = []
	y_train = []
	for i in range(numSamples):

		powerSequence = []
		act = activations[randint(0,len(activations)-1)]
		"""
		if randint(0,100) > 66:
			for i in range(int( len(act)/2) ):
				s = randint(0,len(act)-1)
				p = randint(0,20)/100
				act[s] += act[s]*p if randint(0,1) == 0 else -act[s]*p

		if randint(0,100) > 49:
			s = randint(0,len(act)-1)
			if randint(0,1) == 1:
				act = act[:s]
			else:
				act = act[s:]
		"""
		if len(act) <= lMean:
			s = randint(0,lMean-len(act))

			#powerSequence=[0]*(lMean-len(act))
			powerSequence=[0]*s
			powerSequence.extend(act)
		else:
			#if randint(0,1) == 1:
			#	powerSequence = act[(len(act)-lMean):]
			#else:
			#	powerSequence = act[:-(len(act)-lMean)]
			s = randint(0,len(act)-lMean)
			#powerSequence=[0]*(lMean-len(act))
			powerSequence=act[s:s+lMean]
			#powerSequence.extend(act)
		powerSequence.extend([0]*(lMean-len(powerSequence)))
		energyProfile = synthesize_energyProfile_fromActivation(powerSequence,energy_interval=window_size)	
		
		posSamples.append(energyProfile)
		y_train.append(1)
	
	print("Ne:",lMean/window_size)
	print(np.shape(posSamples))
	"""
	print("len energyProfile",len(energyProfile))
	x_train,y_train = build_windows(energyProfile,1,window_size=18)
	"""
	return posSamples,y_train


def get_negativeSamples(posSamples,numSamples):
	negSamples = []
	y = []
	window_size = len(posSamples[0])


	for i in range(numSamples):
		powerSequence = [0]*window_size
		act = posSamples[randint(0,len(posSamples)-1)]
		
		if randint(0,1) == 1:
			powerSequence[randint(0,len(powerSequence)-1)] = act[randint(0,len(act)-1)]
		else:
			#a = copy.copy(act)
			shuffle(powerSequence)
			#shuffle(a)
			#powerSequence = a
		
		negSamples.append(powerSequence)
		#y.append([0]*len(powerSequence))
		y.append(0)
	return negSamples,y

def crossover_samples(numSamples,x1,x2,y1,y2):

	new_samples = []
	labels = []
	for i in range(numSamples):
		s1 = randint(0,len(x1)-1)
		s2 = randint(0,len(x2)-1)
		sample = []
		for i in range(int(len(x1[s1])/2)):
			position_x1 = randint(0,len(x1[s1])-1)
			position_x2 = randint(0,len(x2[s2])-1)

			x1[s1][position_x1],x2[s2][position_x2] = x2[s2][position_x2],x1[s1][position_x1]
			y1[s1][position_x1],y2[s2][position_x2] = y2[s2][position_x2],y1[s1][position_x1]
		
		new_samples.append(x1[s1])
		new_samples.append(x2[s2])
		labels.append(y1[s1])
		labels.append(y2[s2])

	return new_samples,labels




def get_samples(numSamples,appliance,window_size):
	
	x_train,y_train = get_positiveSamples(numSamples,appliance,window_size)
	
	x,y = get_negativeSamples(posSamples=x_train,numSamples=numSamples)
	
	#x1,y1 = crossover_samples(numSamples,copy.copy(x),copy.copy(x_train),copy.copy(y),copy.copy(y_train))
	
	x_train.extend(x)
	y_train.extend(y)

	#x_train.extend(x1)
	#y_train.extend(y1)

	c = list(zip(x_train,y_train))

	shuffle(c)

	x_train,y_train = zip(*c)
	
	x_train = normalize(x_train, axis=0)

	plt.subplot(331)
	plt.title(y_train[0])
	plt.plot(x_train[0])
	plt.subplot(332)
	plt.title(y_train[1])
	plt.plot(x_train[1])
	plt.subplot(333)
	plt.title(y_train[2])
	plt.plot(x_train[2])
	plt.subplot(334)
	plt.title(y_train[3])
	plt.plot(x_train[3])
	plt.subplot(335)
	plt.title(y_train[4])
	plt.plot(x_train[4])
	plt.subplot(336)
	plt.title(y_train[5])
	plt.plot(x_train[5])
	plt.subplot(337)
	plt.title(y_train[6])
	plt.plot(x_train[6])
	plt.subplot(338)
	plt.title(y_train[7])
	plt.plot(x_train[7])
	plt.subplot(339)
	plt.title(y_train[8])
	plt.plot(x_train[8])
	#plt.show()
	#exit()

	x_train = np.array(x_train,dtype=float).reshape(np.shape(x_train)[0],np.shape(x_train)[1],1)
	y_train = np.array(y_train,dtype=float)

	return x_train,y_train


def train_classification(appliance,numSamples,window_size):
	x_train,y_train = get_samples(numSamples,appliance,window_size)

	print("x_train shape:", np.shape(x_train))
	print("y_train shape:", np.shape(y_train))
	
	print(x_train[0])
	print(y_train[0])
	
	model = nn.build_model_PostProcessing(inputSize=len(x_train[0]),arch=2)
	print(model.summary())
	#exit()
	
	model.compile(loss='binary_crossentropy',optimizer='adam',metrics=['accuracy'])
	#model.compile(loss='cosine_proximity',optimizer='adam',metrics=['accuracy'])
	es = EarlyStopping(monitor='val_acc', mode='max', verbose=2,patience=20)

	history = model.fit(x_train, y_train,verbose=1,batch_size=128,epochs=300,validation_split=0.2,callbacks=[es])
	model.save('models/postProcessing_model_'+appliance+'.h5')

	exit()
	plt.subplot(211)
	plt.plot(history.history['accuracy'])
	plt.plot(history.history['val_accuracy'])
	plt.title('model accuracy')
	plt.ylabel('accuracy')
	plt.xlabel('epoch')
	plt.legend(['train', 'test'], loc='upper left')
	plt.subplot(212)
	# summarize history for loss
	plt.plot(history.history['loss'])
	plt.plot(history.history['val_loss'])
	plt.title('model loss')
	plt.ylabel('loss')
	plt.xlabel('epoch')
	plt.legend(['train', 'test'], loc='upper left')
	plt.show()

def train_regression(appliance,numSamples,window_size):
	x_train,y_train = get_samples(numSamples,appliance,window_size)

	print("x_train shape:", np.shape(x_train))
	print("y_train shape:", np.shape(y_train))
	
	print(x_train[0])
	print(y_train[0])
	
	model = nn.build_model_PostProcessing(inputSize=len(x_train[0]),arch=2)
	print(model.summary())
	#exit()
	
	model.compile(loss='binary_crossentropy',optimizer='adam',metrics=['accuracy'])
	#model.compile(loss='cosine_proximity',optimizer='adam',metrics=['accuracy'])
	es = EarlyStopping(monitor='val_acc', mode='max', verbose=2,patience=20)

	history = model.fit(x_train, y_train,verbose=1,batch_size=128,epochs=300,validation_split=0.2,callbacks=[es])
	model.save('models/postProcessing_model_'+appliance+'.h5')

	exit()
	plt.subplot(211)
	plt.plot(history.history['accuracy'])
	plt.plot(history.history['val_accuracy'])
	plt.title('model accuracy')
	plt.ylabel('accuracy')
	plt.xlabel('epoch')
	plt.legend(['train', 'test'], loc='upper left')
	plt.subplot(212)
	# summarize history for loss
	plt.plot(history.history['loss'])
	plt.plot(history.history['val_loss'])
	plt.title('model loss')
	plt.ylabel('loss')
	plt.xlabel('epoch')
	plt.legend(['train', 'test'], loc='upper left')
	plt.show()


def use(timeSerie,appliance):

	model = load_model('models/postProcessing_model_'+appliance+'.h5')
	inputSize = model.inputs[0].shape[1]

	point = 0
	print(model.summary())
	print(inputSize)
	filtered = []
	for i in range(inputSize,len(timeSerie),inputSize):
		print(i)
		print(len(timeSerie))
		plt.plot(timeSerie[point:i])
		
		a = np.array(timeSerie[point:i]).reshape(len(timeSerie[point:i]),1)
		#a = normalize(a, axis=1)
		a =  np.expand_dims(a, axis=0)
		#print(np.shape(a))
		#print(a)
		b = model.predict(a,batch_size=1 )
		print(b)
		#c = timeSerie[point:i]
		#for j in range(len(c)):
		#	if b[0][j] < 0.5:
		#		c[j] = 0

		#filtered.extend(c)
		
		if b > 0.5:
			print('True')
			filtered.extend(timeSerie[point:i])
		else:
			print('False')
			filtered.extend([0]*(i-point))
		
		point = i
		#plt.show()
	return filtered


def use_test(timeSerie,appliance):

	model = load_model('models/postProcessing_model_'+appliance+'.h5')
	
	tam = len(timeSerie)
	point = 0
	inputSize = model.inputs[0].shape[1]
	filtered = []
	while point+inputSize < tam:
	
		p = point
		point = find_next_window(timeSerie,point,inputSize)
		filtered.extend(timeSerie[p:point])
		plt.plot(timeSerie[point:point+inputSize])
		print('point:',point)	
		a = np.array(timeSerie[point:point+inputSize]).reshape(len(timeSerie[point:point+inputSize]),1)
		a = np.expand_dims(a, axis=0)
		b = model.predict(a,batch_size=1)
		
		if b > 0.5:
			print("True")
			filtered.extend(timeSerie[point:point+inputSize])
		else:
			print("False")
			filtered.extend([0]*inputSize)

		#plt.show()
		point+=inputSize
	
	return filtered

def find_next_window(timeSerie,point,inputSize):

	for i in range(point,len(timeSerie)):
		if i+inputSize >= len(timeSerie):
			return i

		if timeSerie[i] <= 0.01 and timeSerie[i+inputSize] <= 0.01:
			return i



if __name__ == '__main__':
	train_classification(appliance='dish washer',numSamples=10000,window_size=100)






