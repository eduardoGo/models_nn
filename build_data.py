import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import IsolationForest
import os
import matplotlib.pyplot as plt

dir_path = str(os.path.dirname(os.path.realpath(__file__)))

def get_data_AMPDS(appliance):

	data = {}
	
	location = dir_path+'/data/AMPds/Electricity_'+str(appliance)+'.csv'
	df_data = pd.read_csv(location)

	data[appliance] = list(map(float,df_data['P'].values))

	location = dir_path+'/data/AMPds/Electricity_WHE.csv'
	df_data = pd.read_csv(location)

	data['Aggregate'] = list(map(float,df_data['P'].values))

	#pre processing
	data['Aggregate'] = [data['Aggregate'][i]-200 for i in range(len(data['Aggregate']))]
	data['Aggregate'] = [data['Aggregate'][i] if data['Aggregate'][i]>0 else 0 for i in range(len(data['Aggregate']))]

	
	return data


def get_data_andMain(appliance,house,sample1minute=False,time=False):

	data = {}
	if sample1minute:
		location = dir_path+'/data/ukdale/House'+str(house)+'/'+str(appliance)+'AndMainComplete_1minute.csv'
	else:
		location = dir_path+'/data/ukdale/House'+str(house)+'/'+str(appliance)+'AndMainComplete.csv'
	
	df_data = pd.read_csv(location)

	data[appliance] = list(map(float,df_data['Desaggregate'].values))
	data['Aggregate'] = list(map(float,df_data['Aggregate'].values))


	if time:
		data["Time"] = list(df_data['Time'].values)

	return data


def get_data_complete(appliance,house):
	
	dir_path = str(os.path.dirname(os.path.realpath(__file__)))

	data = {}
	location = dir_path+'/data/ukdale/House'+str(house)+'/'+str(appliance)+'.csv'
	data[appliance] = list(map(float,pd.read_csv(location)['power'][1:]))

	#location = dir_path+'/data/ukdale/House'+str(house)+'/Aggregate.csv'
	#data['Aggregate'] = list(map(float,pd.read_csv(location)['power'][1:]))

	return data


def load_activations(appliance,house):
	acts = []
	try:
		for i in range(1000):
			with open(dir_path+'/data/ukdale/House'+str(house)+'/acts/acts_'+appliance+'_House'+str(house)+'/'+str(i)+'.txt','r') as arquivo:
				act = []
				for line in arquivo:
					currentPlace = line[:-1]
					#print(currentPlace)
					act.append(float(currentPlace))
				acts.append(act)
	except FileNotFoundError:
		return acts

	return acts
	

'''
Retorna um dicionario onde cada chave é o nome do dispositivo e cada chave contem uma lista com o consumo do respectivo dispositivo.
As listas possuem tamanho (end - start).
'''
def get_data_size(appliances,house,start,end):
	
	data = {}

	dir_path = str(os.path.dirname(os.path.realpath(__file__)))
	for app in appliances:
		#location = dir_path+'/data/ukdale/House'+str(house)+'/'+str(app)+'AndMainComplete.csv'
		#data[app] = list(map(float,pd.read_csv(location)['Desaggregate'][start:end]))
		location = dir_path+'/data/ukdale/House'+str(house)+'/'+str(app)+'AndMainComplete.csv'
		data[app] = list(map(float,pd.read_csv(location)["Desaggregate"][start:end]))

	location = dir_path+'/data/ukdale/House'+str(house)+'/Aggregate.csv'
	data['Aggregate'] = list(map(float,pd.read_csv(location)['Power apparent'][start:end]))
	return data



def detect_outliers(data,outliers_fraction):

	df_data = data.copy()

	scaler = StandardScaler()
	np_scaled = scaler.fit_transform(data)
	data = pd.DataFrame(np_scaled)

	model =  IsolationForest(contamination=outliers_fraction)
	model.fit(data)

	df_data['anomaly2'] = pd.Series(model.predict(data))
	# df['anomaly2'] = df['anomaly2'].map( {1: 0, -1: 1} )

	outliers = df_data.loc[df_data['anomaly2'] == -1,['index','Aggregate']] #anomaly

	removed_outliers = []
	for i in range(len(df_data['anomaly2'].values)):
		if df_data['anomaly2'].values[i] != -1:
			removed_outliers.append(df_data['Aggregate'].values[i])
		else:
			removed_outliers.append(removed_outliers[-1])
	df_data['Removed outliers'] = pd.Series(removed_outliers)


	del df_data['anomaly2']

	return df_data

def buildWindows(time_serie,window_size,pass_):

	windows = []
	trainY = []

	for i in range(0,len(time_serie),pass_):  # Building windows
		if i+window_size > len(time_serie):
			break
		windows.append(time_serie[i:i+window_size])


	return windows

def downSample(appliance):
	house = 1
	data = get_data_andMain(appliance,house,time=True)
	aggr = []
	time = ['1723 17236']
	desag = []
	flag = False
	for i in range(len(data['Aggregate'])):
		if int(data['Time'][i][-2:]) >= 0 and int(data['Time'][i][-2:]) < 10:
			if int(data['Time'][i][-5:-3]) != int(time[-1][-5:-3]):
				aggr.append(data['Aggregate'][i])
				time.append(data['Time'][i])
				desag.append(data[appliance][i])

	del time[0]	
	dfMain = pd.DataFrame(aggr)
	dfFridge = pd.DataFrame(desag)
	dfTime = pd.DataFrame(time)

	dir_path = str(os.path.dirname(os.path.realpath(__file__)))
	location = dir_path+'/data/ukdale/House'+str(house)+'/'+appliance+'AndMainComplete_1minute.csv'
	result = pd.concat([dfTime,dfMain,dfFridge],ignore_index=True,axis=1)
	result.to_csv(location,header=['Time','Aggregate','Desaggregate'],index=False)
		
def tests():
	location = dir_path+'/data/CLEAN_REFIT_081116/CLEAN_House1.csv'
	df = pd.read_csv(location)

	plt.subplot(211)
	plt.plot(df['Aggregate'][:1000000],label='Aggregate',color='#FBFE1E')
	plt.plot(df['Appliance6'][:1000000],label='dish washer',color='#CE187B')
	plt.legend()

	filterr = []
	for i in range(500,500000):
		if df['Appliance6'][i] > df['Aggregate'][i]:
			df['Aggregate'][i] += df['Appliance6'][i]
			#print('Penality in i:',i)

	plt.subplot(212)
	plt.plot(df['Aggregate'][:1000000],label='Aggregate',color='#FBFE1E')
	plt.plot(df['Appliance6'][:1000000],label='dish washer',color='#CE187B')
	plt.legend()
	plt.show()
	


'''
def run():
	appliances = ['washing machine']

	data = pd.DataFrame(get_data(appliances,house = 4,start=3700,end=4700))
	data = data.reset_index()
	print(data)

	print(detect_outliers(data,outliers_fraction=0.02))
'''

if __name__ == '__main__':
	downSample('microwave')
	downSample('kettle')
	downSample('fridge')
