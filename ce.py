import build_data as bd
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
#import colorednoise as cn
from scipy.stats import uniform
import os

os.environ["CUDA_VISIBLE_DEVICES"]="1"
from nilmtk import DataSet
import os
from itertools import permutations


# Fisher Information
# probs: vector of probabilities from the Bandt-Pompe distribution
def fisher_information(probs):

	# the number of probabilities
	n = len(probs)

	# the normalization constant
	f0 = 1/2

	probs_ = list(probs.values())
	f = sum([ (math.sqrt(probs_[i+1])-math.sqrt(probs_[i]))**2 for i in range(n-1)])
	f = f0*f

	return f




def get_symbols(dimension):
	'''
	return a matrix in the shape (!D,D)
	where on each row we have one pattern.
	'''

	a = [i for i in range(dimension)]

	return np.array(list(permutations(a))).reshape(math.factorial(dimension),dimension)


def get_wedding(patterns):
	'''
	return a wedding list
	the wedding list associate each index of one pattern to each element in the data.

	the wedding[i] has the index of the patterns that the data[i] have.
	'''

	#patterns = patterns + 1

	m = np.shape(patterns)[0]
	D = np.shape(patterns)[1]
	symbols = get_symbols(D)
	
	wedding = [0]*m

	for i in range(m):
		j = 0
		while j < math.factorial(D):

			#if the all elements of the lists Symbols and Patterns are equal, so the sum is D
			if(sum(symbols[j,] == patterns[i,]) == D):
				wedding[i] = j
				break

			j = j + 1


	return wedding


def formationPattern(data,dimension,delay,option):
	'''
	return one matrix in the format:
		[index, D0,D1,...,D]
		where the sublist [D0,D1,...,D] represents the pattern in the data[index]
	'''

	i = 0
	n = len(data)
	#p_patterns = np.zeros( (n,dimension))
	p_patterns = np.array([float('nan')]*(n*dimension)).reshape(n,dimension)
	elements = np.array([float('nan')]*(n*dimension)).reshape(n,dimension)
	index = [0]*(dimension-1)
	
	while i + ((dimension-1)*delay) < n :

		# the indices for the subsequence
		ind = slice( i, i+(dimension)*delay, delay)

		# get the sub-timeseries (sliding window)
		sub = data[ind]
		elements[i,] = sub
		
		p_patterns[i,] = list(np.argsort(sub))
		i+=1
	
	
	p_patterns = p_patterns[~np.isnan(p_patterns)].reshape(-1,dimension)
	elements = elements[~np.isnan(elements)].reshape(-1,dimension)

	return p_patterns,elements


def build_graph_wtag(series,dimension,delay):
	graph = np.zeros((math.factorial(dimension),math.factorial(dimension)))

	patterns,elements = formationPattern(series, dimension, delay, 0)
	wedding = get_wedding(patterns)

	m = len(wedding)
	weight_total = 0

	for i in range(m-1):
	  #graph[wedding[i],wedding[i+1]] += 1

	  weight_i1 = (max(elements[i,]) - min(elements[i,]))
	  weight_i2 = (max(elements[i+1,]) - min(elements[i+1,]))
	  graph[wedding[i],wedding[i+1]] = graph[wedding[i],wedding[i+1]] + abs(weight_i1 - weight_i2)
	  weight_total = weight_total + abs(weight_i1 - weight_i2)

	graph = graph/weight_total
	return graph


def plot_heatMAP(matrix):
	plt.imshow(matrix, cmap='hot', interpolation='nearest')
	plt.show()


def autocorr_manual(time_serie,lag):
	time_serie = list(time_serie)
	corr = 0
	n = len(time_serie)
	mean = np.mean(time_serie)
	std = np.std(time_serie)
	
	for i in range(n-lag):
		corr += (time_serie[i]-mean)*(time_serie[i+lag]-mean)


	corr = corr*(1/( (n-lag)*(std**2) ))
	return corr



def build_matrix(serie,lag_max):
	matrix = np.zeros((lag_max, lag_max))

	for i in range(lag_max):
		for j in range(i,lag_max):
			matrix[i][j] = serie.autocorr(j-i)
			#matrix[i][j] = autocorr_manual(serie,j-i)
			if pd.isnull(matrix[i][j]):
				matrix[i][j] = 0
			matrix[j][i] = matrix[i][j]

	return matrix
	plot_heatMAP(matrix)
	exit()

def eigenvalues(matrix):
	#return np.linalg.svd(matrix,compute_uv=False)
	return np.linalg.eigvals(matrix)
	


def normalizeEigenvalues(eigenvalues,lag_max):

	#histogram
	p = []

	if sum(eigenvalues) == 0:
		p = [0]*len(eigenvalues)
		return p

	sum_ = sum(eigenvalues)
	for i in range(len(eigenvalues)):
		p.append(eigenvalues[i]/(sum_))

	#plt.subplot(211)
	#plt.title('P')
	#plt.bar(p.keys(), p.values(), width=0.3, color='g')

	return p


#Calcula H[P]
def normalized_entropy(p,dict_flag,normalized):
	
	if dict_flag:
		interable = p
	else:
		interable = range(len(p))
	
	h=0
	for i in interable:
		if p[i] > 0:
			h += p[i]*math.log(p[i])

	if normalized and len(p) > 1:
		h = (1/math.log(len(p)))*h

	return -h

def jensen_Shannon_divergence(p,dict_flag):

	if dict_flag:
		pe = [1/len(p.values()) for i in range(len(p.values()))]
		interable = zip(p.values(),pe)
	else:
		pe = [1/len(p) for i in range(len(p))]
		interable = zip(p,pe)


	#X = (P+Pe)/2
	x = [(a+b)/2 for a,b in interable]

	#H[(P+Pe)/2]
	hppe = normalized_entropy(x,dict_flag=False,normalized=False)

	#print("Computing H[P]/2...")	
	hp = normalized_entropy(p,dict_flag=dict_flag,normalized=False)

	#print("Computing H[Pe]/2...")
	hpe = normalized_entropy(pe,dict_flag=False,normalized=False)

	#print("Computing H[J,Je]...")
	jppe = hppe-hp/2-hpe/2

	return jppe

def median_filter(time_series,window_size=4):


	for i in range(int(window_size/2),len(time_series)-1-int(window_size/2)):
		time_series[i] = np.median(time_series[i-int(window_size/2):i+int(window_size)])

	return time_series

def runFisherxShannon(appliance,house):

	dir_path = str(os.path.dirname(os.path.realpath(__file__)))
	ukdale = DataSet(dir_path+'/data/ukdale/ukdale.h5')
	acts = ukdale.buildings[house].elec[appliance].get_activations()
	
	if len(acts) > 50:
		acts = acts[:50]
	

	fisInformation = []
	shaEntropy = []
	for i in range(np.shape(acts)[0]):

		print("Sample ",i," from ", np.shape(acts)[0])
		
		#if appliance == 'fridge' or appliance == 'kettle':
		#activation = median_filter(pd.Series(acts[i]))
		#else:
		activation = pd.Series(acts[i])
		
		if len(activation) < 60:
			continue
		
		p = build_histogram_bandt_pompe(activation)
		
		#H[P]
		hp = normalized_entropy(p,dict_flag=True,normalized=True)
		
		
		fisInformation.append(fisher_information(p))
		shaEntropy.append(hp)

	return shaEntropy,fisInformation


def runJensenShannonDivergence(appliance,house):

	dir_path = str(os.path.dirname(os.path.realpath(__file__)))
	ukdale = DataSet(dir_path+'/data/ukdale/ukdale.h5')
	acts = ukdale.buildings[house].elec[appliance].get_activations()
	
	if len(acts)> 50:
		acts = acts[:50]

	jessenShannon = []
	shaEntroppy = []
	for i in range(np.shape(acts)[0]):

		print("Sample ",i," from ", np.shape(acts)[0])
		
		#if appliance == 'fridge' or appliance == 'kettle':
		#	sample_timeSerie = median_filter(pd.Series(acts[i]))
		#else:
		sample_timeSerie = pd.Series(acts[i])
		
		if len(sample_timeSerie) < 60:
			continue

		p = build_histogram_bandt_pompe(sample_timeSerie)
		
		#Calculate the entropy of the P
		hp = normalized_entropy(p,dict_flag=True,normalized=True)

		#Calculate J[P,Pe]
		jppe = jensen_Shannon_divergence(p,dict_flag=True)

		#Normalization constant
		k = (0.5)/len(p)
		a1 = (0.5 + k) * math.log(0.5 + k)
		a2 = (len(p) - 1) * k * math.log(k)
		a3 = (1 - 0.5) * math.log(len(p))
		q0 = -1/(a1 + a2 + a3)
		
		#Q[P,Pe] = Q0*J[P,Pe]
		qppe = q0*jppe

		jessenShannon.append(qppe*hp)
		shaEntroppy.append(hp)

		#if hp <= 0.43 and c[hp] >= 0.2 and c[hp] <= 0.28 and hp >= 0.25:
		#plt.title(str(hp) + ' C: '+str(c[hp])+'\n'+appliance)
		#plt.plot(sample_timeSerie)
		#plt.matshow(graph)
		#plt.show()


	return shaEntroppy,jessenShannon



def runEGCI(appliance,house,lag_max=256):
	
	dir_path = str(os.path.dirname(os.path.realpath(__file__)))
	ukdale = DataSet(dir_path+'/data/ukdale/ukdale.h5')
	acts = ukdale.buildings[house].elec[appliance].get_activations()
	
	if len(acts)> 50:
		acts = acts[:50]
	
	egci = []
	shaEntropy = []
	for i in range(np.shape(acts)[0]):

		print("Sample ",i," from ", np.shape(acts)[0])
		
		#if appliance == 'fridge' or appliance == 'kettle':
		#	sample_timeSerie = median_filter(pd.Series(acts[i]))
		#else:
		sample_timeSerie = pd.Series(acts[i])
		
		if len(sample_timeSerie) < 60:
			continue
		
		#Calcula a matriz de correlação de dimensão lag_max x lag_max
		autocrr_matrix = build_matrix(sample_timeSerie,lag_max)

		#Calcula os autovalores da matriz
		ev_list = eigenvalues(autocrr_matrix)


		p = normalizeEigenvalues(ev_list,lag_max)

		#Calcula a entropia do histograma H[P]
		hp = normalized_entropy(p,dict_flag=False,normalized=True)
	
		#Calculate J[P,Pe]
		jppe = jensen_Shannon_divergence(p,dict_flag=False)

		#Constante de normalização
		k = (0.5)/lag_max
		a1 = (0.5 + k) * math.log(0.5 + k)
		a2 = (lag_max - 1) * k * math.log(k)
		a3 = (1 - 0.5) * math.log(lag_max)
		q0 = -1/(a1 + a2 + a3)
		
		#Q[P,Pe] = Q0*J[P,Pe]
		qppe = q0*jppe

		egci.append(qppe*hp)
		shaEntropy.append(hp)


	return shaEntropy,egci



def runWATG(appliance,house):
	
	dir_path = str(os.path.dirname(os.path.realpath(__file__)))
	ukdale = DataSet(dir_path+'/data/ukdale/ukdale.h5')
	acts = ukdale.buildings[house].elec[appliance].get_activations()
	
	if len(acts)> 50:
		acts = acts[:50]
	
	complexity = []
	shaEntropy = []
	for i in range(np.shape(acts)[0]):

		print("Sample ",i," from ", np.shape(acts)[0])
		
		#if appliance == 'fridge' or appliance == 'kettle':
		#	sample_timeSerie = median_filter(pd.Series(acts[i]))
		#else:
		sample_timeSerie = pd.Series(acts[i])
		
		if len(sample_timeSerie) < 60:
			continue
		
		
		graph = build_graph_wtag(sample_timeSerie,dimension=3,delay=1)
		p = list(graph.flat)
		

		#Calcula a entropia do histograma H[P]
		hp = normalized_entropy(p,dict_flag=False,normalized=True)
		
		#calcula J[P,Pe]
		jppe = jensen_Shannon_divergence(p,dict_flag=False)

		#Constante de normalização
		k = (0.5)/len(p)
		a1 = (0.5 + k) * math.log(0.5 + k)
		a2 = (len(p) - 1) * k * math.log(k)
		a3 = (1 - 0.5) * math.log(len(p))
		q0 = -1/(a1 + a2 + a3)
		
		#Q[P,Pe] = Q0*J[P,Pe]
		qppe = q0*jppe

		complexity.append(qppe*hp)
		shaEntropy.append(hp)

		#if hp <= 0.43 and c[hp] >= 0.2 and c[hp] <= 0.28 and hp >= 0.25:
		#plt.title(str(hp) + ' C: '+str(c[hp])+'\n'+appliance)
		#plt.plot(sample_timeSerie)
		#plt.matshow(graph)
		#plt.show()


	return shaEntropy, complexity


def build_histogram_bandt_pompe(data, D=4, tau=1, by=1):

	# the list of symbols to be returned
	symbols = {}

	# dicovering the sequences of order n
	for s in range(1, len(data)-(D-1)*tau, by):

		# the indices for the subsequence
		ind = slice( s, s+(D-1)*tau, tau)

		# get the sub-timeseries (sliding window)
		sub = data[ind]

		
		# the current permutation pattern
		pattern = ''.join(list(map(str,np.argsort(sub))))
		#pattern = paste(, collapse='')
		
		# adding the current pattern to the list of symbols
		if not pattern in symbols:
			symbols[pattern] = 1
		else:
			symbols[pattern] += 1

	#normalize
	if sum(symbols.values()) > 0:
		sum_ = sum(symbols.values())**(-1)
	else:
		sum_=0
	for s in symbols:
		symbols[s] = symbols[s]*sum_
	

	return symbols

def normalize_data(dict_data):

	for key in dict_data:
	
		time_serie = list(dict_data[key])
		
		maxValue = max(time_serie)
		minValue = min(time_serie)

		for i in range(len(time_serie)):
			time_serie[i] = (time_serie[i]-minValue)/(maxValue-minValue)

		dict_data[key] = time_serie

	return dict_data

def build_plane(function,appliances,houses,plotParamets):

	colors = ['black','blue','pink','red','yellow', 'green','Gray','Gold','#991100','#590325','#893568','#123456','#696889']
	markers = ['o','x','^','p','v','*','s','+','X',"P","*","h","H","+","x" ,"X","D"]

	for house in houses:
		for j in range(len(appliances)):
			x,y = function(appliance=appliances[j],house=house)
			print("Appliance:",appliances[j])
			plt.scatter(x,y,marker=markers[j*2+1],color=colors[j*2+1],label=appliances[j])
			
			
			
	plt.legend()
	plt.title(plotParamets['title'])
	plt.xlabel(plotParamets['xlabel'])
	plt.ylabel(plotParamets['ylabel'])
	plt.xlim(0,1)
	plt.ylim(0,1)
	# plt.figure(num=None, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
	# plt.savefig(appliance[0]+'_'+houses[0]+'.png')
	plt.show()

houses = [2]
appliances = ['dish washer','fridge', 'kettle', 'microwave', 'washing machine', 'running machine']
#appliance = ['dish washer']

plotParamets= {'title':'Complexidade x Entropia\nWATG',
				'xlabel':'H',
				'ylabel':'C'}


#Quantidade de pontos no plano HxC
samples = 64

#Tamanho de cada amostra do sinal (cada amostra será um ponto do plano HxC)
size_samples = 512

build_plane(function=runWATG,appliances=appliances,houses=houses,plotParamets=plotParamets)

# plotParamets= {'title':'Complexidade x Entropia\nEGCI',
# 				'xlabel':'H',
# 				'ylabel':'C',

# build_plane(function=runEGCI,appliances=appliances,houses=houses,plotParamets=plotParamets)


lag_max = 128
# plotParamets= {'title':'Complexidade x Entropia\nNormal',
# 				'xlabel':'H',
# 				'ylabel':'C'}


# build_plane(function=runJensenShannonDivergence,appliances=appliances,houses=houses,plotParamets=plotParamets)


		
		
plt.legend()
		

plt.title("Plano HxC")
plt.xlabel('H')
plt.ylabel('C')
#plt.xlim(0,1)
#plt.ylim(0,1)
#plt.figure(num=None, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
plt.savefig(appliance[0]+'_'+str(houses[0])+'.png')
#plt.show()

# plotParamets= {'title':'Complexidade x Entropia\nFisher-Shannon',
# 				'xlabel':'H',
# 				'ylabel':'F'}

# build_plane(function=runFisherxShannon,appliances=appliances,houses=houses,plotParamets=plotParamets)
