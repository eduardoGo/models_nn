import nn as nn
import build_data as bd
#import outliers_data as ot
import data_sampling as ds
import math
from keras.models import load_model
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import copy
from keras import backend as K
from sklearn.metrics import f1_score
import scipy.stats as st

plt.style.use('ggplot')
os.environ["CUDA_VISIBLE_DEVICES"]="0"
dir_path = str(os.path.dirname(os.path.realpath(__file__)))

def b2uild_windows(time_serie,window_size):

	windows_Y = []
	

	for i in range(0,len(time_serie),window_size):  # Building windows
		windows_Y.append(sum(time_serie[i:i+window_size]))
	
	return windows_Y
def main(window_size,appliance):

	dataTest = bd.get_data_andMain(appliance,house=2)

	dataTest["Desaggregate"] = dataTest.pop(appliance)

	#dataTest['Aggregate'] = dataTest['Aggregate'][1936259:]
	#dataTest['Desaggregate'] = dataTest['Desaggregate'][1936259:]

	dataTest['Aggregate'] = dataTest['Aggregate'][:60000*16]
	dataTest['Desaggregate'] = dataTest['Desaggregate'][:60000*16]

	dataTest = ds.reduce_data(copy.copy(dataTest),times = 4)

	plt.plot(b2uild_windows(dataTest['Desaggregate'],window_size),'--o',label=appliance)

	return

if __name__ == '__main__':
	
	apps = ['active speaker',
	'broadband router', 
	'computer',
	'external hard disk',
	'laptop computer',
	'running machine',
	'toaster',
	'dish washer',
	'fridge',
	'kettle',
	'washing machine']
	#'microwave',
	'''
	plt.suptitle("Média do consumo na janela", fontsize=14)
	for j in range(len(apps)):
		plt.subplot(4,3,j+1)
		window_size = 400
		main(window_size=window_size,appliance=apps[j])			
		plt.legend()

	plt.show()
	'''

	f1 = []