from random import randint,shuffle,random
import nn as nn
import build_data as bd
#import outliers_data as ot
#import data_sampling as ds
import math
from keras.optimizers import Adam,SGD
from keras.models import load_model
import numpy as np
import pandas as pd
from Early_Stopping import Early_Stopping
import os
import copy
#import matplotlib.pyplot as plt
import post_processing_regress as pp
from datetime import datetime
from keras import backend as K

WINDOW_SIZE = 301
APPLIANCE = 'microwave'
HOUSE  = 1

os.environ["CUDA_VISIBLE_DEVICES"]="0"
dir_path = str(os.path.dirname(os.path.realpath(__file__)))

def mish(inputs):
	return inputs * K.tanh(K.softplus(inputs))

def synthesize_aggregate(data,activations):
	original_aggregate = data['Aggregate']
	pointer = 0
	tam = len(original_aggregate)

	n = random()
	tw = (1-n)/n
	#tw *= pp.get_tamMean(activations)
	tw *= 1000
	print("TW:",tw)

	artificial_aggregate = []
	while pointer < tam:
		r = int(np.random.exponential(scale=tw,size=1)[0])
		act = activations[randint(0,len(activations)-1)]

		if len(artificial_aggregate)+r > tam or len(artificial_aggregate)+r+len(act) > tam:
			artificial_aggregate.extend([0]*(tam - len(artificial_aggregate)))
			break
		else:
			artificial_aggregate.extend([0]*r)
			artificial_aggregate.extend(act)

		pointer+=r+len(act)

	data['Aggregate'] = [sum(x) for x in zip(original_aggregate, artificial_aggregate)]
	data['Desaggregate'] = [sum(x) for x in zip(data['Desaggregate'], artificial_aggregate)]

	return data


def normalize_data(dict_data):

	
	#maxValue = max(max(dict_data['Aggregate']), max(dict_data['Desaggregate']))
	#minValue = min(min(dict_data['Aggregate']), min(dict_data['Desaggregate']))

	

	for key in dict_data:
	
		time_serie = list(dict_data[key])
		
		maxValue = max(time_serie)
		minValue = min(time_serie)

		for i in range(len(time_serie)):
			time_serie[i] = (time_serie[i]-minValue)/(maxValue-minValue)

		dict_data[key] = time_serie

	return dict_data

def build_windows(data,window_size,pass_):

	windows_X = []
	windows_Y = []
	

	for i in range(0,len(data["Aggregate"]),pass_):  # Building windows
		
		if i+window_size > len(data["Aggregate"]):
			break
		windows_X.append(data['Aggregate'][i:i+window_size])
		#windows_Y.append(data["Desaggregate"][i:i+window_size])
		windows_Y.append([ data["Desaggregate"][i+int(window_size/2)-1],data["Desaggregate"][i+int(window_size/2)],data["Desaggregate"][i+int(window_size/2)+1]])

	#print(np.shape(windows_Y))
	#print(np.shape(windows_X))
	#exit()
	if np.shape(windows_Y)[0] > 0:
		return np.array(windows_X).reshape(np.shape(windows_X)[0],np.shape(windows_X)[1],1),np.array(windows_Y)
	#return np.array(windows_X),np.array(windows_Y)
	return windows_X,windows_Y
	

def median_filter(time_series,window_size):


	for i in range(int(window_size/2),len(time_series)-1-int(window_size/2)):
		time_series[i] = np.median(time_series[i-int(window_size/2):i+int(window_size)])

	return time_series


def myLoss(y_act,y_pred):
	return K.sum(abs(y_pred-y_act))/K.sum(y_act)

def train_nn(model,data,testX,testY,batch_size=256):



	samples = np.shape(data['Aggregate'])[0]

	interations_epoch = int(samples/batch_size)

	epochs = 2000
		
	#scheduler_lr = scheduler_learningRate(lower_lr=0.0006,upper_lr=0.0057,interations_epoch = interations_epoch,max_epochs = epochs)
	
	history = {'loss':[],'val_loss':[]}

	#opt = Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
	
	#model.compile(optimizer='adam', loss='mse')
	model.compile(optimizer='adam', loss=myLoss)

	early_stopping = Early_Stopping(max_epochs = 200,min_loss = model.evaluate(np.array(testX), np.array(testY)))

	for epoch in range(epochs):

		#learn_rate = scheduler_lr.get_lr(epoch)

		#opt = Adam(lr=learn_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
		#opt = SGD(lr=learn_rate)

		#model.compile(optimizer='adam', loss='mse')

		print("Epoch",epoch,"of",epochs)
		#print("Learning rate:",learn_rate)


		start_point = 0
		end_point = 0




		#Split the data and train
		#Dessa forma, nos dividimos os dados de forma que a cada interação desse loop, temos xtrain com um total de 1000000 de dados
		for k in range(int(3000000/WINDOW_SIZE),len(data['Aggregate']), int(3000000/WINDOW_SIZE)):

			start_point = end_point
			end_point = k
			current_data = {}
			current_data['Aggregate'] = data['Aggregate'][start_point:end_point]
			current_data['Desaggregate'] = data['Desaggregate'][start_point:end_point]
			#current_data['Timetable'] = data['Timetable'][start_point:end_point]
			#plt.plot(current_data['Aggregate'],label='Aggregate')
			#plt.plot(current_data['Desaggregate'],label='Desaggregate')
			#plt.show()
			trainX,trainY = build_windows(current_data,WINDOW_SIZE,pass_ = 1) #Build windows to train neural network.
			
			current_loss = model.fit(trainX,trainY,verbose=0,batch_size=batch_size,epochs=1)
			

		print("#####################################################################################")
		print("#####################################################################################")
		print("#####################################################################################")
		print("#####################################################################################")
		print("#####################################################################################")
		print("#####################################################################################")
		current_valLoss = model.test_on_batch(x = testX,y = testY)

		print('loss:',current_loss.history['loss'][0],'val_loss',current_valLoss)
		
		history['loss'].append(current_loss.history['loss'][0])
		
		history['val_loss'].append(current_valLoss)

		#history['learn_rate'].append(learn_rate)

		if early_stopping.count_loss(loss = current_valLoss,model = model, path_save = dir_path+'/models/best_model_'+APPLIANCE+'_UKDALE_Mish.h5'):
			print("Val_loss do not improved in {0} epochs".format(early_stopping.max_epochs))
			print("Epoch",epoch,"of",epochs)
			print("Stopping ...")
			break

		#model.save(dir_path+'/models/model_'+APPLIANCE+'_UKDALE_Mish.h5')
		#model.save(dir_oath+'models/model_')
		model.save(dir_path+'/models/model_'+APPLIANCE+'_BACKUP.h5')
		#model.save(dir_path+'/models/model_'+APPLIANCE+'_Complete_woSyn.h5')
	#plot_loss(history)
	pd.DataFrame(history).to_csv('history.csv')

def get_data_AMPds(appliance):

	dir_path = str(os.path.dirname(os.path.realpath(__file__)))

	location = dir_path+'/data/AMPds'
	data = pd.read_csv(location+'/Electricity_WHE.csv')[['unix_ts','P']]
	data['Aggregate'] = data.pop('P')

	#Convert timestamp to date (str)
	data['unix_ts'] = list(map(lambda x: datetime.utcfromtimestamp(int(x)).strftime('%Y-%m-%d %H:%M:%S'),data['unix_ts'].values))
	data["Time"] = data.pop('unix_ts') #Rename unix_ts to Time

	data[appliance] = pd.read_csv(location+'/Electricity_'+appliance+'.csv')['P']
	data["Desaggregate"] = data.pop(appliance)

	#Training period starts at 18 August 2012 and ends at 13 April 2013;
	#30 days were used as test sample (17 May 2013-17 June 2013).
	for i in range(len(data['Time'])):
		
		if data['Time'][i][:10] == '2012-08-17':
			b_train = i+1
		
		if data['Time'][i][:10] == '2013-04-12':
			e_train = i+1
		
		if data['Time'][i][:10] == '2013-05-16':
			b_test = i+1
		
		if data['Time'][i][:10] == '2013-06-16':
			e_test = i+1
			break
		
	data = data[['Aggregate',"Desaggregate"]]

	data_train = data.iloc[b_train:e_train]
	data_test = data.iloc[b_test:e_test]

	return data_train,data_test

def get_data_UKDALE():
	data = bd.get_data_andMain(APPLIANCE,HOUSE) #Get data. It's return the dictionary that contais aggregate APPLIANCE and desaggregate data
	dataTest = bd.get_data_andMain(APPLIANCE,house=2)

	#Rename key, APPLIANCE to Desaggregate
	data["Desaggregate"] = data.pop(APPLIANCE)
	dataTest["Desaggregate"] = dataTest.pop(APPLIANCE)

	dataTest['Aggregate'] = dataTest["Aggregate"][1936259:]
	dataTest['Desaggregate'] = dataTest["Desaggregate"][1936259:]

	return data,dataTest

model = nn.build_architect(WINDOW_SIZE,features=1,arch=3) #Construct neural network without compile
#model = load_model(dir_path+'/models/model_'+APPLIANCE+'_UKDALE_Mish.h5',custom_objects={'mish':mish,'myLoss':myLoss})
#model = load_model(dir_path+'/models/best_model_'+APPLIANCE+'_Early_woSyn.h5')
#model = load_model(dir_path+'/models/model_'+APPLIANCE+'_BACKUP.h5')

print(model.summary())
data,dataTest = get_data_UKDALE()

#data,dataTest = get_data_AMPds(APPLIANCE)

data = synthesize_aggregate(data,pp.get_activations(APPLIANCE,HOUSE))

#Normalize the data to a range from 0 to 1
data = normalize_data(data)
dataTest = normalize_data(dataTest)

testX,testY = build_windows(dataTest,WINDOW_SIZE,pass_ = 1) #Build windows to train neural network.


train_nn(model,data,testX,testY,batch_size=256)

