import nn as nn
import build_data as bd
#import outliers_data as ot
#import data_sampling as ds
import math
from keras.models import load_model
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import copy
import post_processing as pp
from random import shuffle
from sklearn.metrics import f1_score

WINDOW_SIZE = 100
APPLIANCE = 'washing machine'
HOUSE = 2
#QT_AMOSTRAS = 1602
DEBUG = False

os.environ["CUDA_VISIBLE_DEVICES"]="1"
dir_path = str(os.path.dirname(os.path.realpath(__file__)))

def test_acc(y_hat,y):

	dif = 0
	den = 0
	for i in range(np.shape(y_hat)[0]):
		#if y_hat[i][0] < 200:
		#	y_hat[i][0] = 0.8
		dif += abs(y_hat[i] - y[i][0])
		den+=y[i][0]

	return 1 - dif/(2*den)


def calc_f1(y_hat,y):

	y_hat_label = []
	y_label = []
	for i in range(np.shape(y_hat)[0]):
		if y_hat[i] > 200:
			y_hat_label.append(1)
		else:
			y_hat_label.append(0)

		if y[i] > 200:
			y_label.append(1)
		else:
			y_label.append(0)

	return f1_score(y_label,y_hat_label)

def metricsMAE(y_pred,y_true):
    
    mae = 0
    
    for i in range(min(len(y_pred),len(y_true))):
        mae += abs(y_true[i]-y_pred[i])

    return mae/len(y_pred)

def metricSAE(y_pred,y_true):

    return abs(sum(y_true) - sum(y_pred))/sum(y_true)

def metricsSAEdelt(y_pred,y_true):
    rHat = sum(y_pred)
    r = sum(y_true)

    saeDelt = abs(r - rHat)/np.shape(y_true)[0]
   
    return saeDelt

def myLossFunc(y_act,y_pred):
        return K.sum(abs(y_pred-y_act))#/(2*sum(y_act))




def des_normalize(time_serie,minValue,maxValue,flag=False):

	#return time_serie

	for i in range(len(time_serie)):
		if flag:
			time_serie[i] = time_serie[i][0]
		time_serie[i] = time_serie[i]*(maxValue-minValue) + minValue
		#if time_serie[i] < 0:
		#	time_serie[i] = 0


	return time_serie

def normalize_data(dict_data):

	maxValue = max(max(dict_data['Aggregate']), max(dict_data['Desaggregate']))
	minValue = min(min(dict_data['Aggregate']), min(dict_data['Desaggregate']))
	

	for key in dict_data:
	
		time_serie = dict_data[key]

		for i in range(len(time_serie)):
			time_serie[i] = (time_serie[i]-minValue)/(maxValue-minValue)

		dict_data[key] = time_serie

	return dict_data,minValue,maxValue

def build_windows(data,window_size,pass_):

	windows_X = []
	windows_Y = []
	
	for i in range(0,int(len(data["Aggregate"])),pass_):  # Building windows
		
		if i+window_size > len(data["Aggregate"]):
			break
		
		'''
		auxList = []
		for j in range(window_size):
			auxList.append([data['Aggregate'][i+j],data['Timetable'][i+j]])
		'''
		windows_X.append(data['Aggregate'][i:i+window_size])
		windows_Y.append([sum(data['Desaggregate'][i:i+window_size])/window_size])


	c = list(zip(windows_X,windows_Y))
	shuffle(c)
	windows_X,windows_Y = zip(*c)

	return np.array(windows_X).reshape(np.shape(windows_X)[0],np.shape(windows_X)[1],1),np.array(windows_Y)

def evaluate(model,data,minValue,maxValue,batch_size):
		
	testX,testY = build_windows(data,WINDOW_SIZE,pass_ = WINDOW_SIZE) #Build windows to train neural network.
	print("Shape of testX:",testX.shape)
	#testX,testY = testX[int(len(testX)*0.5):,:],testY[int(len(testY)*0.5):,:]
	print("Shape of testX:",testX.shape)
	
	predict = model.predict(testX,batch_size=batch_size)

	if DEBUG:
		plt.subplot(311)
		plt.plot(predict.reshape(-1),color='blue',label='Consumo Máquina de lavar roupa inferido')
		plt.plot(testY.reshape(-1),'--',color='orange',label='Consumo Máquina de lavar roupa')
		plt.legend()
		plt.subplot(312)
		plt.plot(predict.reshape(-1),color='blue',label='Consumo Máquina de lavar roupa inferido')
		plt.legend()
		plt.subplot(313)
		plt.plot(testY.reshape(-1),color='orange',label='Consumo Máquina de lavar roupa')
		plt.legend()		
		plt.show()
		exit()
	
	testY = des_normalize(testY,minValue,maxValue,flag=True)
	
	print(np.shape(predict))
	print(np.shape(testY))
		
	predict = des_normalize(predict,minValue,maxValue,flag=False)
	
	for i in range(len(predict)):
		if predict[i] < 0:
			predict[i] = 0

	ea_after = test_acc(predict,testY)
	print("ea after",ea_after)
	
	print("F1 score:", calc_f1(y_hat=predict,y=testY))
	
	
	print(np.shape(predict))
	print(np.shape(testY))
	plt.plot(testY,label='Consumo real',color='red')
	plt.plot(predict,'--',label='Consumo inferido with',color='blue')
	plt.legend()
	plt.show()


#model = load_model(dir_path+'/models/model_'+APPLIANCE+'_Complete_BACKUP.h5')
#model = load_model(dir_path+'/models/model_'+APPLIANCE+'_EXPERIMENT1.h5',compile=False)
model = load_model(dir_path+'/models/best_model_'+APPLIANCE+'_Early.h5',compile=False)
#model = load_model(dir_path+'/models/model_007_1.h5')

print(model.summary())

dataTest = bd.get_data_andMain(APPLIANCE,HOUSE,False)

#Reduced data to 1/2^TIMES_REDUCTION of the your lenght originally utilizing wavelet transform. 

dataTest["Desaggregate"] = dataTest.pop(APPLIANCE)

#tam = 174967 #sample rate = 1min
tam = 1750092 #sample rate = 6s
dataTest['Aggregate'] = dataTest['Aggregate'][tam:]
dataTest['Desaggregate'] = dataTest['Desaggregate'][tam:]


#dataTest['Aggregate'] = dataTest['Aggregate'][-QT_AMOSTRAS*WINDOW_SIZE:]
#dataTest['Desaggregate'] = dataTest['Desaggregate'][-QT_AMOSTRAS*WINDOW_SIZE:]


minValue,maxValue = 0,0
dataTest,minValue,maxValue = normalize_data(dataTest)

evaluate(model,dataTest,minValue,maxValue,batch_size=128)
