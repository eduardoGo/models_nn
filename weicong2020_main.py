import nn as nn
import build_data as bd
#import outliers_data as ot
import data_sampling as ds
import math
from keras.optimizers import Adam,SGD
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from Early_Stopping import Early_Stopping
from keras.models import load_model
from keras.callbacks import LearningRateScheduler,ReduceLROnPlateau,ModelCheckpoint,EarlyStopping
import os
import copy
import time
from keras import backend as K
import tensorflow as tf
from random import randint,shuffle,random
import post_processing as pp
from sklearn.metrics import f1_score
DEBUG = False

os.environ["CUDA_VISIBLE_DEVICES"]="0"
dir_path = str(os.path.dirname(os.path.realpath(__file__)))

def lr_schedule(epoch):
    """Learning Rate Schedule

    Learning rate is scheduled to be reduced after 80, 120, 160, 180 epochs.
    Called automatically every epoch as part of callbacks during training.

    # Arguments
        epoch (int): The number of epochs

    # Returns
        lr (float32): learning rate
    """
    lr = 1e-3
    if epoch == 50:
        lr *= 1e-1
    elif epoch == 100:
        lr *= 1e-1
    elif epoch == 200:
        lr *= 1e-1
    elif epoch == 300:
        lr *= 1e-1
    return lr

def test_acc(model,x,y):
	y_hat = model.predict(x)

	dif = 0
	den = 0
	for i in range(x.shape[0]):
		dif += abs(y_hat[i][0] - y[i][0])
		den+=y[i][0]

	return dif/(2*den)

def calc_f1(model,y,x):
	window_size = 400
	thrsh = 200/5467.1934

	y_hat_label = [] 
	y_hat = model.predict(x)
	y_label = []
	for i in range(np.shape(y_hat)[0]):
		
		if y_hat[i] > thrsh:
			y_hat_label.append(1)
		else:
			y_hat_label.append(0)

		if y[i] > thrsh:
			y_label.append(1)
		else:
			y_label.append(0)

	return f1_score(y_label,y_hat_label)

def myLossFunc(y_act,y_pred):
	return K.sum(abs(y_pred-y_act))

def normalize_data(dict_data):

	maxValue = max(max(dict_data['Aggregate']), max(dict_data['Desaggregate']))
	minValue = min(min(dict_data['Aggregate']), min(dict_data['Desaggregate']))
	
	#print(maxValue,minValue)
	#exit()
	for key in dict_data:
	
		time_serie = dict_data[key]
		for i in range(len(time_serie)):
			time_serie[i] = (time_serie[i]-minValue)/(maxValue-minValue)

		dict_data[key] = time_serie

	return dict_data

def find_next_window(current_point,timeSerie,treshold_diff):

	while current_point+1 < len(timeSerie):
		print(abs(timeSerie[current_point]-timeSerie[current_point+1])/0.1)
		if abs(timeSerie[current_point]-timeSerie[current_point+1])/0.1 > treshold_diff:
			return current_point
		else:
			current_point+=1

	return current_point

def build_windows_2(data,window_size,pass_):
	windows_X = []
	windows_Y = []
	windows_Z = []
	
	ttest = []
	point = 0
	point = find_next_window(point,data['Aggregate'],10000)
	
	while point+window_size < len(data['Aggregate']):

		windows_Z.append(data['Desaggregate'][point:point+window_size])
		windows_X.append(data['Aggregate'][point:point+window_size])
		windows_Y.append([sum(data['Desaggregate'][point:point+window_size])/window_size])
		
		
		ttest.append(windows_Y[-1][0])

		point = find_next_window(point+window_size,data['Aggregate'],10000)
	
	if DEBUG:
		plt.plot(ttest,color='orange',label='Consumo Máquina de lavar roupa mtd 2')
		plt.legend()
		plt.show()
		exit()

	c = list(zip(windows_X,windows_Y))
	shuffle(c)
	windows_X,windows_Y = zip(*c)

	return np.array(windows_X).reshape(np.shape(windows_X)[0],np.shape(windows_X)[1],1),np.array(windows_Y)
		
		




def build_windows(data,window_size,pass_):

	windows_X = []
	windows_Y = []

	if DEBUG:
		windows_Z = []
		ttest = []
		count = 0



	for i in range(0,int(len(data["Aggregate"])),pass_):  # Building windows
		
		if i+window_size > len(data["Aggregate"]):
			break
		
		'''
		auxList = []
		for j in range(window_size):
			auxList.append([data['Aggregate'][i+j],data['Timetable'][i+j]])
		'''
		windows_X.append(data['Aggregate'][i:i+window_size])
		windows_Y.append([sum(data['Desaggregate'][i:i+window_size])/window_size])
		if DEBUG:
			windows_Z.append(data['Desaggregate'][i:i+window_size])
			ttest.append(windows_Y[-1][0])

	if DEBUG:
		plt.plot(ttest,color='orange',label='Consumo Máquina de lavar roupa')
		plt.legend()
		plt.show()
		exit()

	'''
	Shuffle the data
	'''
	c = list(zip(windows_X,windows_Y))
	shuffle(c)
	windows_X,windows_Y = zip(*c)

	return np.array(windows_X).reshape(np.shape(windows_X)[0],np.shape(windows_X)[1],1),np.array(windows_Y)


def median_filter(time_series,window_size):


	for i in range(int(window_size/2),len(time_series)-1-int(window_size/2)):
		time_series[i] = np.median(time_series[i-int(window_size/2):i+int(window_size)])

	return time_series

def synthesize_aggregate(data,activations):
	original_aggregate = data['Aggregate']
	pointer = 0
	tam = len(original_aggregate)

	n = random()
	tw = (1-n)/n
	tw *= pp.get_tamMean(activations)
	print("TW:",tw)

	artificial_aggregate = []
	while pointer < tam:
		r = int(np.random.exponential(scale=tw,size=1)[0])
		act = activations[randint(0,len(activations)-1)]

		if len(artificial_aggregate)+r > tam or len(artificial_aggregate)+r+len(act) > tam:
			artificial_aggregate.extend([0]*(tam - len(artificial_aggregate)))
			break
		else:
			artificial_aggregate.extend([0]*r)
			artificial_aggregate.extend(act)

		pointer+=r+len(act)

	data['Aggregate'] = [sum(x) for x in zip(original_aggregate, artificial_aggregate)]
	data['Desaggregate'] = [sum(x) for x in zip(data['Desaggregate'], artificial_aggregate)]

	return data






def train_nn(model,trainX,trainY,testX,testY,history,appliance,batch_size,epochs,early):

	'''
	lower_lr = 0.0006
	upper_lr = 0.0057
	'''
	
	#learn_rate = lr_schedule(0)
	#opt = Adam(lr=learn_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
	
	#model.compile(optimizer=opt, loss='mse')
	#model.compile(optimizer='adam', loss=myLossFunc)
	model.compile(optimizer='adam',loss='mse')

	if early == None:
		early_stopping = Early_Stopping(max_epochs = epochs+1,min_loss = float('inf'))
	else:
		early_stopping = Early_Stopping(max_epochs = early,min_loss = float('inf'))

	#epochs=1
	for epoch in range(epochs):
		
		#l = lr_schedule(epoch)
		#if l != learn_rate:
		#	learn_rate = l
		#	opt = Adam(lr=learn_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
			#model.compile(optimizer=opt, loss=)

		print("Epoch",epoch,"of",epochs)
		#print("Learning rate:",learn_rate)
		
		current_loss = model.fit(x=trainX,y=trainY,batch_size=batch_size,verbose=2,epochs=1)

		#current_valLoss = model.test_on_batch(x = testX,y = testY)
		current_valLoss = test_acc(model,x = testX,y = testY)

		print('val_loss',current_valLoss)
		print('Estimated acc:',1 - current_valLoss)
		
		history['loss'].append(current_loss.history['loss'][0])
		
		history['val_loss'].append(current_valLoss)

		#history['learn_rate'].append(learn_rate)
		
		if early_stopping.count_loss(loss = current_valLoss,model = model, path_save = dir_path+'/models/best_model_'+appliance+'_Early.h5'):
			print("Val_loss do not improved in {0} epochs".format(early_stopping.max_epochs))
			print("Epoch",epoch,"of",epochs)
			print("Stopping ...")
			break

		model.save(dir_path+'/models/model_'+appliance+'_EXPERIMENT1.h5')

	
	return history,model,1-early_stopping.min_loss,calc_f1(model,y=testY,x=testX) 



def main(model, window_size, appliance,house,batch_size,epochs,early):

	if not DEBUG:

		'''
		Build or load model
		'''
		if model == None:
			model = nn.build_architect_test(window_size,features=1,arch=1) #Construct neural network without compile
		else:
			model = load_model(dir_path+'/models/model_'+appliance+'_EXPERIMENT1.h5',compile=False)
		#model = nn.build_architect(window_size,features=1,restNet=True)
		#model = load_model(dir_path+'/models/ResNetS3PL_model_'+appliance+'_BACKUP.h5')
		#model = nn.build_architect_1(window_size=window_size,features=1,restNet=False)

		print(model.summary())
	'''
	Get data. 
	It's return the dictionary that contais aggregate appliance and desaggregate data
	'''
	data_list = []
	for i in house:
		aux = bd.get_data_andMain(appliance,i,sample1minute=True,time=True)
		aux["Desaggregate"] = aux.pop(appliance)
		data_list.append(aux)
	
	
	#Extend e unify data
	data = {'Aggregate':[],'Desaggregate':[],'Time':[]}
	for i in data_list:
		data['Aggregate'].extend(i['Aggregate'])
		data['Desaggregate'].extend(i['Desaggregate'])
		data['Time'].extend(i['Time'])
	del data_list
	"""
	for i in range(len(data['Aggregate'])):
		if data['Time'][i][:16] == '2013-09-20 00:40':
			#print(data['Aggregate'][i:i+1939])
			#print(data['Desaggregate'][i:i+1939])
			#print(data['Time'][i:i+1939])
			#print(i)
			break
		if data['Time'][i][:16] == '2013-07-30 04:55':
			break
			print(i)
	"""	
	del data['Time']
	#exit()

	'''
	É importante testar a rede neural apenas com dados reais, sem a adição dos dados sinteticos, por isso,
	logo abaixo os dados já são divididos.
	'''
	#print(len(data['Aggregate']))
	#exit()
	#tam = 1602*window_size
	
	
	#tam = 1750092 #sample rate = 6s
	#tam = 174967 #sample rate = 1min
	dataTest = {}
	#Separando os dados de teste da casa 5(f=1min) e logo depois excluindo eles para deixar só os de treino
	'''
	dataTest['Aggregate'] = data['Aggregate'][12915:34062]
	dataTest['Desaggregate'] = data['Desaggregate'][12915:34062]

	del data['Aggregate'][12915:34062]
	del data['Desaggregate'][12915:34062]
	'''
	#Separando os dados de teste da casa 2(f=1min) e logo depois excluindo eles para deixar só os de treino
	
	
	dataTest['Aggregate'] = data['Aggregate'][175010:]
	dataTest['Desaggregate'] = data['Desaggregate'][175010:]
	
	del data['Aggregate'][175010:]
	del data['Desaggregate'][175010:]
	
	'''
	#Separando os dados de teste da casa 1(f=1min) e logo depois excluindo eles para deixar só os de treino
	dataTest['Aggregate'] = data['Aggregate'][581891:706227]
	dataTest['Desaggregate'] = data['Desaggregate'][581891:706227]

	del data['Aggregate'][581891:706227]
	del data['Desaggregate'][581891:706227]

	print(len(dataTest['Aggregate']))
	
	#data['Aggregate'] = data['Aggregate'][1618397:1701997]
	#data['Desaggregate'] = data['Desaggregate'][1618397:1701997]
	
	
	#del data['Aggregate'][12915:34062]
	#del data['Desaggregate'][12915:34062]
	'''
	'''
	#tam_b = 760808 #sample rate = 6s
	#tam_e = 1003812 #Sample rate = 6s
	#tam_b = 75603 #sample rate = 1min (inicio)
	#tam_e = 100083 #sample rate = 1min (final)
	#data['Aggregate'] = data['Aggregate'][tam_b:tam_e]
	#data['Desaggregate'] = data['Desaggregate'][tam_b:tam_e]
	'''

	#Normalize the data to a range from 0 to 1
	dataTest = normalize_data(dataTest)	
	#testX,testY = build_windows_2(dataTest,window_size,pass_ = window_size)
	testX,testY = build_windows(dataTest,window_size,pass_ = window_size)	
	
	#Construção dos dados sintéticos
	if len(data['Aggregate'])/window_size < 20000:
		original_data = copy.deepcopy(data)
		data = synthesize_aggregate(data,pp.get_activations(appliance,house[0])) 
		while len(data['Aggregate'])/window_size < 20000:
			prev_data = copy.deepcopy(original_data)
			prev_data = synthesize_aggregate(prev_data,pp.get_activations(appliance,house[0]))
			data['Aggregate'].extend(prev_data['Aggregate'])
			data['Desaggregate'].extend(prev_data['Desaggregate'])
	else:
		#data['Aggregate'] = data['Aggregate'][:10000]
		#plt.plot(data['Aggregate'],label='before')
		data = synthesize_aggregate(data,pp.get_activations(appliance,house[0]))
		#plt.plot(data['Aggregate'],label='after')
		#plt.legend()
		#plt.show()
		#exit()
	
	#Normalize the data to a range from 0 to 1
	data = normalize_data(data)


	'''
	Apply median filter in the data.
	
	dataTest['Aggregate'] = median_filter(dataTest['Aggregate'],window_size=5)
	dataTest['Desaggregate'] = median_filter(dataTest['Desaggregate'],window_size=5)

	data['Aggregate'] = median_filter(data['Aggregate'],window_size=5)
	data['Desaggregate'] = median_filter(data['Desaggregate'],window_size=5)
	'''


	'''
	Build windows for TEST and TRAIN.
	'''
	trainX,trainY = build_windows(data,window_size,pass_ = window_size) #Build windows to train neural network.

	#testX,testY = trainX[int(len(trainX)*0.9):,:],trainY[int(len(trainY)*0.9):,:]
	#trainX,trainY = trainX[: int(len(trainX)*0.9),:],trainY[: int(len(trainY)*0.9),:]

	print("Shape of testX:",testX.shape)
	print("Shape of trainX:",trainX.shape)

	print("Shape of testY:",testY.shape)
	print("Shape of trainY:",trainY.shape)

	
	
	history = {'loss':[],'val_loss':[]}

	'''
	Train model
	'''
	history,model,acc,f1f1 = train_nn(model,trainX,trainY,testX,testY,history,appliance,batch_size,epochs,early)
	pd.DataFrame(history).to_csv('history_'+appliance+'.csv')
	K.clear_session()
	tf.keras.backend.clear_session()
	#f1_result = calc_f1(model,y_label=testY,x=testX) 
	#del dataTest
	del data
	del trainX,trainY,testX,testY,history
	#pd.DataFrame(history).to_csv('history.csv')

	return model,acc,f1f1

if __name__ == '__main__':
	app = 'washing machine'
	print("###################################")
	print("TREINAMENTO IRÁ INICIAR")
	print("###################################")
	acc = []
	f1_arr = []
	b = time.time()
	qt_amostras = 10
	for i in range(qt_amostras):
		m,acc_temp,f1_temp = main(None, window_size=100, appliance=app,house=[1],batch_size=128,epochs=1000,early=20)
		acc.append(acc_temp)
		f1_arr.append(f1_temp)
		print("Amostra",i)
		print("Acuracia:",acc_temp)
		print("F1 score:",f1_temp)
	totalTime = time.time() - b
	print("Tempo total gasto", totalTime/3600, ' hrs')
	print("EC MÉDIO:",np.mean(acc))
	print("STD:",np.std(acc))

	print("F1-SCORE MÉDIO:",np.mean(f1_arr))
	print("STD:",np.std(f1_arr))

	

