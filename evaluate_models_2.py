import nn as nn
import build_data as bd
#import outliers_data as ot
#import data_sampling as ds
import math
from keras.models import load_model
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import copy
import post_processing_regress as pp
from sklearn.metrics import f1_score
from datetime import datetime
from keras import backend as K

WINDOW_SIZE = 301
APPLIANCE = 'washing machine'
APPLIANCE = 'kettle'
APPLIANCE = 'fridge'
APPLIANCE = 'microwave'
APPLIANCE = 'dish washer'
#APPLIANCE = 'CDE'

os.environ["CUDA_VISIBLE_DEVICES"]="0"
dir_path = str(os.path.dirname(os.path.realpath(__file__)))

def myLoss(y_act,y_pred):
	return K.sum(abs(y_pred-y_act))

def mish(inputs):
        return inputs * K.tanh(K.softplus(inputs))

def RMSE(y_pred,y_true):
	
	rmse = 0

	for i in range(min(len(y_pred),len(y_true))):
		rmse += (y_true[i]-y_pred[i])**2

	rmse = math.sqrt(rmse/len(y_pred))
	nrmse = rmse/(max(y_true)-min(y_true))
	return rmse,nrmse

def metricsMAE(y_pred,y_true):
    
    mae = 0
    
    for i in range(min(len(y_pred),len(y_true))):
        mae += abs(y_true[i]-y_pred[i])

    return mae/len(y_pred)

def metricSAE(y_pred,y_true):

    return abs(sum(y_true) - sum(y_pred))/sum(y_true)

def metricsSAEdelt(y_pred,y_true):
    rHat = sum(y_pred)
    r = sum(y_true)

    saeDelt = abs(r - rHat)/np.shape(y_true)[0]
   
    return saeDelt

def test_acc(y_hat,y):

	dif = 0
	den = 0
	for i in range(np.shape(y_hat)[0]):
		#if y_hat[i][0] < 200:
		#	y_hat[i][0] = 0.8
		dif += abs(y_hat[i] - y[i])
		den+=y[i]

	return 1 - dif/(2*den)


def calc_f1(y_hat,y):

	y_hat_label = []
	y_label = []
	for i in range(np.shape(y_hat)[0]):
		if y_hat[i] > 100:
			y_hat_label.append(1)
		else:
			y_hat_label.append(0)

		if y[i] > 100:
			y_label.append(1)
		else:
			y_label.append(0)

	return f1_score(y_label,y_hat_label)

def median_filter(time_series,window_size):


	for i in range(int(window_size/2),len(time_series)-1-int(window_size/2)):
		time_series[i] = np.median(time_series[i-int(window_size/2):i+int(window_size)])

	return time_series

def des_normalize(time_serie,minValue,maxValue):

	#return time_serie

	for i in range(len(time_serie)):
		time_serie[i] = time_serie[i]*(maxValue-minValue) + minValue
		#if time_serie[i] < 0:
		#	time_serie[i] = 0


	return time_serie

def normalize_data(dict_data):

	values_normalize = {}

	maxValue = max(max(dict_data['Aggregate']), max(dict_data['Desaggregate']))
	minValue = min(min(dict_data['Aggregate']), min(dict_data['Desaggregate']))
	
	print(maxValue,minValue)
	#exit()
	for key in dict_data:
	
		time_serie = list(dict_data[key])
		#maxValue = max(time_serie)
		#minValue = min(time_serie)
		
		values_normalize[key] = [maxValue,minValue]

		for i in range(len(time_serie)):
			
			time_serie[i] = (time_serie[i]-minValue)/(maxValue-minValue)
		
		dict_data[key] = time_serie

	return dict_data,values_normalize

def build_windows(data,window_size,pass_):

        windows_X = []
        windows_Y = []

        for i in range(0,len(data["Aggregate"]),pass_):  # Building windows
               

            windows_X.append(data['Aggregate'][i:i+window_size])
            #windows_Y.append(data["Desaggregate"][i:i+window_size])
            windows_Y.append([ data["Desaggregate"][i+int(window_size/2)-1],data["Desaggregate"][i+int(window_size/2)],data["Desaggregate"][i+int(window_size/2)+1]])
            if i+window_size == len(data["Aggregate"]):
                break


        
        #exit()
        if np.shape(windows_Y)[0] > 0:
                return np.array(windows_X).reshape(np.shape(windows_X)[0],np.shape(windows_X)[1],1),np.array(windows_Y)
        #return np.array(windows_X),np.array(windows_Y)
        return windows_X,windows_Y

def median_filter(time_series,window_size):


	for i in range(int(window_size/2),len(time_series)-1-int(window_size/2)):
		time_series[i] = np.median(time_series[i-int(window_size/2):i+int(window_size)])

	return time_series

def buildConsumptionCurve(predict):

    curve = []

    rate = [1,1,1]

    curve.append(predict[0][0])
    curve.append(predict[0][1])
    curve.append(predict[0][2])

    for i in range(1,np.shape(predict)[0]-1):

        curve[i] = (curve[i] + rate[1]*predict[i][0])/2
        curve[i+1] = (curve[i+1]+rate[2]*predict[i][1])/2

        curve.append(predict[i][2])

    del curve[0]

    return curve


def evaluate(model,data,values_normalize,batch_size,delay_window):


	start_point = 0
	end_point = int(1000000/WINDOW_SIZE)
	cts = 0
	time_serie_inferr = []
	for k in range(int(1000000/WINDOW_SIZE),len(data['Aggregate']), int(1000000/WINDOW_SIZE)):
			end_point = k

			print(k,'of',len(data['Aggregate']))
			
			current_data = {}
			current_data['Aggregate'] = data['Aggregate'][start_point:end_point]
			current_data['Desaggregate'] = data['Desaggregate'][start_point:end_point]
			#current_data['Timetable'] = data['Timetable'][start_point:end_point]
			
			
			testX,testY = build_windows(current_data,WINDOW_SIZE,pass_ = 1) #Build windows to train neural network.

			if np.shape(testX)[0] > 0:
				predict = model.predict(testX,batch_size=batch_size)
				#print(np.shape(predict))
				#print(np.shape(testX))
				predict = buildConsumptionCurve(predict)
				#print(np.shape(predict))
				#predict[0],predict[1],predict[-1],predict[-2] = 0,0,0,0
				#for l in range(len(predict)):
				#	predict[l] -= 0.25
				#	predict[l] = predict[l] if predict[l] > 0 else 0
				time_serie_inferr.extend(predict)
				'''
				plt.subplot(211)
				plt.plot(predict,label='predicted')
				plt.plot(current_data['Desaggregate'][int(WINDOW_SIZE/2):end_point-int(WINDOW_SIZE/2)],label='real')
				plt.legend()
				plt.subplot(212)
				plt.plot(predict,label='predicted')
				plt.plot(current_data['Aggregate'][int(WINDOW_SIZE/2):end_point-int(WINDOW_SIZE/2)],label='Aggregate')
				plt.legend()
				plt.show()
				'''
				#print(np.shape(time_serie_inferr))

				del predict
			print("cts:",cts)
			
			cts += 1
			if cts == 2123123:
				break

			print("Diff:",end_point-start_point)
			start_point = end_point-WINDOW_SIZE+1
			
			del current_data
			
			



	
	
	data['Desaggregate'] = des_normalize(data['Desaggregate'][int(WINDOW_SIZE/2):end_point-int(WINDOW_SIZE/2)],values_normalize['Desaggregate'][1],values_normalize['Desaggregate'][0])
	data['Aggregate'] = des_normalize(data['Aggregate'][int(WINDOW_SIZE/2):end_point-int(WINDOW_SIZE/2)],values_normalize['Aggregate'][1],values_normalize['Aggregate'][0])

	#for i in range(len(time_serie_inferr)):
	#	if time_serie_inferr[i] < (values_normalize['Desaggregate'][1] - values_normalize['Desaggregate'][1])/(values_normalize['Desaggregate'][0]-values_normalize['Desaggregate'][1]):
	#		time_serie_inferr[i] = 0


	time_serie_inferr_aux = des_normalize(copy.copy(time_serie_inferr),values_normalize['Desaggregate'][1],values_normalize['Desaggregate'][0])
	
	print(np.shape(time_serie_inferr))
	print(np.shape(data['Desaggregate']))

	time_serie_inferr = pp.use(time_serie_inferr,APPLIANCE)
	time_serie_inferr = des_normalize(time_serie_inferr,values_normalize['Desaggregate'][1],values_normalize['Desaggregate'][0])
	
	new_t = len(time_serie_inferr)

	time_serie_inferr_aux = time_serie_inferr_aux[:new_t]
	data['Desaggregate'] = data['Desaggregate'][:new_t]

	print('MAE before:',metricsMAE(y_pred=time_serie_inferr_aux,y_true=data['Desaggregate']))
	print('SAE before:',metricSAE(y_pred=time_serie_inferr_aux,y_true=data['Desaggregate']))
	print('F1 before:',calc_f1(time_serie_inferr_aux,data['Desaggregate']))
	print('EA before:',test_acc(time_serie_inferr_aux,data['Desaggregate']))
	
	print('MAE after:',metricsMAE(y_pred=time_serie_inferr,y_true=data['Desaggregate']))
	print('SAE after:',metricSAE(y_pred=time_serie_inferr,y_true=data['Desaggregate']))
	print('F1 after:',calc_f1(time_serie_inferr,data['Desaggregate']))
	print('EA after:',test_acc(time_serie_inferr,data['Desaggregate']))
	
	rmse,nrmse = RMSE(time_serie_inferr,data['Desaggregate'])

	print("RMSE:",rmse)
	print('NRMSE:',nrmse)

	print(np.shape(time_serie_inferr))
	print(np.shape(data['Desaggregate']))
	
	
	plt.subplot(211)
	plt.title("Before")
	plt.plot(data['Desaggregate'],label='Consumo real',color='red')
	plt.plot(time_serie_inferr_aux,label='Consumo inferido',color='blue')
	plt.legend()
	plt.subplot(212)
	plt.title("After")
	plt.plot(data['Desaggregate'],label='Consumo real',color='red')
	plt.plot(time_serie_inferr,'--',label='Consumo inferido',color='blue')
	plt.legend()
	plt.show()
	
def get_data_AMPds(appliance):

	dir_path = str(os.path.dirname(os.path.realpath(__file__)))

	location = dir_path+'/data/AMPds'
	data = pd.read_csv(location+'/Electricity_WHE.csv')[['unix_ts','P']]
	data['Aggregate'] = data.pop('P')

	#Convert timestamp to date (str)
	data['unix_ts'] = list(map(lambda x: datetime.utcfromtimestamp(int(x)).strftime('%Y-%m-%d %H:%M:%S'),data['unix_ts'].values))
	data["Time"] = data.pop('unix_ts') #Rename unix_ts to Time

	data[appliance] = pd.read_csv(location+'/Electricity_'+appliance+'.csv')['P']
	data["Desaggregate"] = data.pop(appliance)

	#Training period starts at 18 August 2012 and ends at 13 April 2013;
	#30 days were used as test sample (17 May 2013-17 June 2013).
	for i in range(len(data['Time'])):
		
		if data['Time'][i][:10] == '2013-05-16':
			b_test = i+1
		
		if data['Time'][i][:10] == '2013-06-16':
			e_test = i+1
			break
		
	data = data[['Aggregate',"Desaggregate"]]

	data_test = data.iloc[b_test:e_test]

	data_test = {'Aggregate':list(data_test['Aggregate'].values),'Desaggregate':list(data_test['Desaggregate'].values)}

	return data_test

#model = load_model(dir_path+'/models/model_'+APPLIANCE+'_Complete_woSyn.h5')
#model = load_model(dir_path+'/models/model_'+APPLIANCE+'_BACKUP.h5',custom_objects={'mish':mish,'myLoss':myLoss})
model = load_model(dir_path+'/models/best_model_'+APPLIANCE+'_Early_woSyn.h5')
#model = load_model(dir_path+'/models/model_007_1.h5')
#model = load_model(dir_path+'/models/best_model_'+APPLIANCE+'_UKDALE_Mish.h5',custom_objects={'mish':mish})
#model = load_model(dir_path+'/models/model_'+APPLIANCE+'_UKDALE_Mish.h5',custom_objects={'mish':mish,'myLoss':myLoss})
print(model.summary())


dataTest = bd.get_data_andMain(APPLIANCE,house=2)
dataTest["Desaggregate"] = dataTest.pop(APPLIANCE)

dataTest['Aggregate'] = dataTest['Aggregate'][1936259:]
dataTest['Desaggregate'] = dataTest['Desaggregate'][1936259:]

#dataTest['Aggregate'] = dataTest['Aggregate'][100000:200000]
#dataTest['Desaggregate'] = dataTest['Desaggregate'][100000:200000]


#dataTest = get_data_AMPds(APPLIANCE)

minValue,maxValue = 0,0
dataTest,values_normalize = normalize_data(dataTest)

'''
Apply median filter in the data.
'''

#dataTest['Aggregate'] = median_filter(dataTest['Aggregate'],window_size=5)
#dataTest['Desaggregate'] = median_filter(dataTest['Desaggregate'],window_size=5)


#dataTest['Desaggregate'] = np.array(dataTest['Desaggregate'])/1000
#dataTest['Aggregate'] = np.array(dataTest['Aggregate'])/1000


#plt.plot(dataTest['Aggregate'],label='Agregado',color='black')
#plt.plot(dataTest['Desaggregate'],'--',label='real',color='red')
#plt.legend()
#plt.show()
#exit()


evaluate(model,dataTest,values_normalize,batch_size=256,delay_window=1)
