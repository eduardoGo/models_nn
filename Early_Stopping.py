
from keras.models import Model


class Early_Stopping(object):
	"""docstring for Early_Stopping"""
	def __init__(self, max_epochs,min_loss):
		
		self.max_epochs = max_epochs
		self.count = 0
		self.min_loss = min_loss

	def count_loss(self,loss,model,path_save):

		if loss < self.min_loss:
			self.count = 0
			model.save(path_save)
			print("Loss improved of {0} to {1}".format(self.min_loss,loss))
			print("Model saved in",path_save)
			self.min_loss = loss
		else:
			self.count += 1
			print("Loss did not improved")
			print("Tolerance: {0} of {1}".format(self.count,self.max_epochs))
		return True if self.count > self.max_epochs else False



		

