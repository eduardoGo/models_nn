import pandas as pd
#import matplotlib.pyplot as plt
import build_data as bd
from random import randint,shuffle,random
import nn
from keras.models import load_model
from keras.callbacks import EarlyStopping
import numpy as np
import os
from sklearn.preprocessing import normalize
import copy

os.environ["CUDA_VISIBLE_DEVICES"]="0"
dir_path = str(os.path.dirname(os.path.realpath(__file__)))

def find_next_window(current_point,timeSerie,treshold):

	while current_point+1 < len(timeSerie):
		if timeSerie[current_point] > treshold:
			return current_point
		else:
			current_point+=1

	return current_point

def build_windows(timeSerie,window_size):
	windows_X = []
	windows_Y = []
	
	point = 0
	point = find_next_window(point,timeSerie,1000)
	
	while point+window_size < len(timeSerie):
		windows_X.append(timeSerie[point:point+window_size])
		windows_Y.append(1)
		
		point = find_next_window(point+window_size,timeSerie,1000)

	c = list(zip(windows_X,windows_Y))
	shuffle(c)
	windows_X,windows_Y = zip(*c)

	return windows_X,windows_Y


def get_activations(appliance,house):
	acts = []
	if appliance == 'microwave' and house == 5:
		numActs = 4
	if appliance == 'microwave' and house == 1:
		numActs = 5
	if appliance == 'kettle':
		numActs=1
	if appliance == 'dish washer':
		numActs=2
		#a = bd.load_activations(appliance,2)
		#acts.extend(a)
		#a = bd.load_activations(appliance,5)
		#acts.extend(a)
	if appliance == 'washing machine':
		numActs = 4
	
	for i in range(numActs):	
		#with open(dir_path+'/data/acts_'+appliance+'_House'+str(house)+'/'+str(i)+'.txt','r') as arquivo:
		with open(dir_path+'/data/ukdale/House'+str(house)+'/acts/acts_'+appliance+'_House'+str(house)+'/'+str(i)+'.txt','r') as arquivo:
			act = []
			for line in arquivo:
				currentPlace = line[:-1]
				act.append(float(currentPlace))
			acts.append(act)
	
	return acts


def extract_activations(activations,timeSerie,treshold,tamMin):

	if activations == None:
		activations = []

	aux = [0,0]

	for i in range(len(timeSerie)):
		if timeSerie[i] < treshold and len(aux) > 2:
			aux.append(0)
			aux.append(0)
			if len(aux)>tamMin:
				aux.extend(timeSerie[i:i+800])
				activations.append(aux)
			aux = [0,0]
		elif timeSerie[i] > treshold:
			aux.append(timeSerie[i])
	shuffle(activations)
	return activations


def get_tamMean(activations):


	m = 0
	for i in range(len(activations)):
		m+=len(activations[i])

	return m/len(activations)



def get_positiveSamples(numSamples,appliance):

	#data = bd.get_data_andMain(appliance=appliance,house=4)
	#activations = extract_activations(None,data[appliance],treshold=10,tamMin=30)

	#data = bd.get_data_andMain(appliance=appliance,house=2)
	#activations = extract_activations(None,data[appliance],treshold=10,tamMin=30)
	activations = get_activations(appliance,house=2)

	#data = bd.get_data_andMain(appliance=appliance,house=1)
	#activations = extract_activations(activations,data[appliance],treshold=10,tamMin=30)
	
	lMean = int(get_tamMean(activations))
	print("Len mean:",lMean)

	
	window_size = 50

	n = random()
	tw = (1-n)/n
	tw *= 1000
	print("TW:",tw)

	tam = numSamples*window_size
	print('tam:',tam)
	artificial_data = []
	pointer = 0
	while pointer < tam:
		r = int(np.random.exponential(scale=tw,size=1)[0])
		act = activations[randint(0,len(activations)-1)]

		if len(artificial_data)+r > tam or len(artificial_data)+r+len(act) > tam:
			artificial_data.extend([0]*(tam - len(artificial_data)))
			break
		else:
			artificial_data.extend([0]*r)
			artificial_data.extend(act)

		pointer+=r+len(act)

	#plt.plot(artificial_data)
	#plt.show()
	x_train,y_train = build_windows(artificial_data,window_size=window_size)
	
	return x_train,y_train


def get_negativeSamples(posSamples,numSamples):
	negSamples = []
	y = []
	window_size = len(posSamples[0])


	for i in range(numSamples):
		powerSequence = [0]*window_size
		act = posSamples[randint(0,len(posSamples)-1)]
		

		'''
		Cada amostra tem:
		60% de chance de ter de 2 a 1/5 de picos de energia aleatorios
		20% de chance de ter 1 pico aleatorio
		20% de chance de ser a assinatura com picos
		'''
		r = randint(0,100)
		if r > 40:
			qtPicos = randint(2,int(len(powerSequence)/20))
			for j in range(qtPicos):
				powerSequence[randint(0,len(powerSequence)-1)] = act[randint(0,len(act)-1)]*0.5 if randint(0,1) == 0 else act[randint(0,len(act)-1)]
		elif r > 20:
			powerSequence[randint(0,len(powerSequence)-1)] = act[randint(0,len(act)-1)]*0.5 if randint(0,1) == 0 else act[randint(0,len(act)-1)]
		else:
			stam = randint(0,int(len(powerSequence))/2)
			for j in range(stam):
				powerSequence[j] = random()
			#a = copy.copy(act)
			#shuffle(a)
			#powerSequence = a
			#qtPicos = randint(2,int(len(powerSequence)/20))
			#for j in range(qtPicos):
			#	powerSequence[randint(0,len(powerSequence)-1)] = 
			
		negSamples.append(powerSequence)
		#y.append([0]*len(powerSequence))
		y.append(0)
	return negSamples,y


def get_samples(numSamples,appliance):
	
	x_train,y_train = get_positiveSamples(numSamples,appliance)
	
	x_train,y_train = list(x_train),list(y_train)
	x,y = get_negativeSamples(posSamples=x_train,numSamples=len(x_train))
	
	print(np.shape(x_train))
	
	#x_train,y_train = x,y	
	x_train.extend(x)
	y_train.extend(y)

	c = list(zip(x_train,y_train))
	shuffle(c)
	x_train,y_train = zip(*c)
	

	y_train = np.array(y_train).reshape(-1, 1)
	

	print(np.shape(x_train))
	print(np.shape(y_train))
	
	x_train = normalize(x_train, axis=1)
	#y_train = normalize(y_train, axis=0)

	plt.subplot(331)
	plt.title(y_train[0])
	plt.plot(x_train[0])
	plt.subplot(332)
	plt.title(y_train[1])
	plt.plot(x_train[1])
	plt.subplot(333)
	plt.title(y_train[2])
	plt.plot(x_train[2])
	plt.subplot(334)
	plt.title(y_train[3])
	plt.plot(x_train[3])
	plt.subplot(335)
	plt.title(y_train[4])
	plt.plot(x_train[4])
	plt.subplot(336)
	plt.title(y_train[5])
	plt.plot(x_train[5])
	plt.subplot(337)
	plt.title(y_train[6])
	plt.plot(x_train[6])
	plt.subplot(338)
	plt.title(y_train[7])
	plt.plot(x_train[7])
	plt.subplot(339)
	plt.title(y_train[8])
	plt.plot(x_train[8])
	#plt.show()
	#exit()

	x_train = np.array(x_train,dtype=float).reshape(np.shape(x_train)[0],np.shape(x_train)[1],1)
	y_train = np.array(y_train,dtype=float)

	return x_train,y_train


def train(appliance,numSamples):
	x_train,y_train = get_samples(numSamples,appliance)

	print("x_train shape:", np.shape(x_train))
	print("y_train shape:", np.shape(y_train))
	
	#print(x_train[0])
	#print(y_train[0])
	
	model = nn.build_model_PostProcessing(inputSize=len(x_train[0]),arch=3)
	print(model.summary())
	#exit()
	
	model.compile(loss='binary_crossentropy',optimizer='adam',metrics=['accuracy'])
	es = EarlyStopping(monitor='val_accuracy', mode='max', verbose=2,patience=20)

	
	history = model.fit(x_train, y_train,verbose=1,batch_size=128,epochs=300,validation_split=0.2,callbacks=[es])
	model.save('models/postProcessing_model_regress_'+appliance+'_1.h5')

	exit()
	plt.subplot(211)
	plt.plot(history.history['accuracy'])
	plt.plot(history.history['val_accuracy'])
	plt.title('model accuracy')
	plt.ylabel('accuracy')
	plt.xlabel('epoch')
	plt.legend(['train', 'test'], loc='upper left')
	plt.subplot(212)
	# summarize history for loss
	plt.plot(history.history['loss'])
	plt.plot(history.history['val_loss'])
	plt.title('model loss')
	plt.ylabel('loss')
	plt.xlabel('epoch')
	plt.legend(['train', 'test'], loc='upper left')
	plt.show()


def use(timeSerie,appliance):

	model = load_model('models/postProcessing_model_regress_'+appliance+'.h5')
	tam = len(timeSerie)
	print('t',tam)
	point = 0
	inputSize = int(model.inputs[0].shape[1])
	filtered = []
	while True:
		p = point
		point = find_next_window(point,timeSerie,0.01)
		filtered.extend(timeSerie[p:point])
		if point+inputSize > tam:
			break
		#plt.plot(timeSerie[point:point+inputSize])
		print('point:',point)	
		a = np.array(timeSerie[point:point+inputSize]).reshape(-1,1)
		a = np.expand_dims(a, axis=0)
		b = model.predict(a,batch_size=1)
		print(b)
		if b > 0.5:
			print("True")
			filtered.extend(timeSerie[point:point+inputSize])
		else:
			print("False")
			filtered.extend([0]*inputSize)

		#plt.show()
		point+=inputSize
	
	return filtered



if __name__ == '__main__':
	train(appliance='kettle',numSamples=20000)






