import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import copy
import build_data as bd
import pywt
from scipy import stats
from dtw import dtw,accelerated_dtw



#data = pd.DataFrame(bd.get_data(appliances = ['washing machine'],house = 4,start = 40000,end = 40400))
euclidean_norm = lambda x, y: np.abs(x - y)

def reduce_dwt(vector,wave):
    reduced_vector, coefs = pywt.dwt(vector,wave,'symmetric')
    
    return reduced_vector, coefs

def invert_dwt(reduced_vector,wave='db14',times=4):
    
    for i in range(times):
        reduced_vector = pywt.idwt(reduced_vector, cD=None,wavelet=wave, mode='symmetric', axis=-1)


    return reduced_vector

def reduce_data(data,times = 1):
    wave = 'db18'
    '''
    plt.subplot(311)
    plt.title("Coefficients generated \nfrom the aggregated signal",fontsize=20)
    plt.plot(data[1900:2900],label='Aggregated\nSignal',color='#60995C')
    plt.xlabel('Samples',fontsize=20)
    plt.ylabel('Power (W)',fontsize=20)
    a = list(map(str,np.arange(int(max(data[1900:2900]))+1,step=1000)))
    plt.xticks(np.arange(2900-1900+1,step=250),fontsize=20)
    plt.yticks(np.arange(int(max(data[1900:2900]))+1,step=1000),labels=['   '+x for x in a],fontsize=20)
    
    plt.legend(fontsize=15,loc='upper left',bbox_to_anchor=(1.01,1),borderaxespad=0)
    '''
    for i in range(times):
        data,coe = reduce_dwt(data,wave)
    
    #data['Aggregate'] = invert_dwt(data['Aggregate'],'db14',times)

    '''
    print(coe)
    print('-----------------------------------------------------------------------')
    print(data)
    plt.subplot(312)
    plt.plot(data[950:950+500],label='Aproximation\nCoefficients',color='#4B34E6')
    plt.xlabel('Samples',fontsize=20)
    plt.ylabel('Power (W)',fontsize=20)
    
    a = list(map(str,np.arange(int(max(data[950:950+500]))+1,step=1000)))

    plt.xticks(np.arange(950-950+500+1,step=250),fontsize=20)
    plt.yticks(np.arange(int(max(data[950:950+500]))+1,step=1000),labels=['   '+x for x in a],fontsize=20)

    plt.legend(fontsize=15,loc='upper left',bbox_to_anchor=(1.01,1),borderaxespad=0)
    plt.subplot(313)
    plt.plot(coe[950:950+500],label='Detail\nCoefficients',color='#19E60B')
    plt.xlabel('Samples',fontsize=20)
    plt.ylabel('Power (W)',fontsize=20)

    plt.xticks(np.arange(950-950+500+1,step=250),fontsize=20)
    plt.yticks(np.arange(-1000,1001,step=1000),fontsize=20)

    plt.legend(fontsize=15,loc='upper left',bbox_to_anchor=(1.01,1),borderaxespad=0)
    plt.show()
    exit()
    '''
    return data

'''

Wave de menor média: db14
Distancia média: 98.3203019196883


'''
def calculate_smallerDistance_complete(data):

    wavelist = pywt.wavelist(kind = 'discrete')[24:]
    minimal_global_mean = float("inf")
    wave_global_minimal = 'a'
    print("Starting...")
    for wave in wavelist:

        count = 0
        minD_mean = 0
        wave_minD = 'a'
        for i in range(512,len(data['Aggregate'][:100000]),512):
            
            vectorOut_agg, coefs_agg = reduce_dwt(data['Aggregate'][i-512:i],wave)
            #d, cost_matrix, acc_cost_matrix, path = dtw(vectorOut_agg,data['Aggregate'][i-512:i], dist=euclidean_norm)
            d, cost_matrix, acc_cost_matrix, path = accelerated_dtw(np.array(vectorOut_agg),np.array(data['Aggregate'][i-512:i]), dist=euclidean_norm)            
            minD_mean += d

            count += 1


        minD_mean = minD_mean/count


        print("Wave:", wave,"distancia media:",minD_mean)
        if minD_mean < minimal_global_mean:
            minimal_global_mean = minD_mean
            wave_global_minimal = wave


    print("Wave de menor média:",wave_global_minimal)
    print("Distancia média:",minimal_global_mean)

'''
appliance = 'fridge'
print("HOUSE 4---------------------------")
data = bd.get_data_andMain(appliance,house = 4)

calculate_smallerDistance_complete(data)
print("HOUSE 2---------------------------")
data = bd.get_data_andMain(appliance,house = 2)

calculate_smallerDistance_complete(data)
print("HOUSE 1---------------------------")
data = bd.get_data_andMain(appliance,house = 1)

calculate_smallerDistance_complete(data)

exit()
'''
"""
vectorOut_agg, coefs_agg = reduce_dwt(data['Aggregate'].values,wave = 'db35')
vectorOut_des, coefs_agg = reduce_dwt(data['washing machine'].values,wave = 'db35')

'''
db3:
pvalue=3.3306690738754696e-16
pvalue=0.000122017962
db6:
pvalue=1.6653345369377348e-15
pvalue=8.925984510621e-05
'''

d, cost_matrix, acc_cost_matrix, path = dtw(vectorOut_agg,data['Aggregate'].values, dist=euclidean_norm)
print(d)

print(stats.ks_2samp(vectorOut_agg,data['Aggregate'].values))
print(stats.ks_2samp(vectorOut_des,data['washing machine'].values))

plt.subplot(211)
plt.plot(vectorOut_agg,color='red',label='reduzido agregado')
plt.plot(vectorOut_des,color='black',label='reduzido desagregado')
plt.legend()
plt.subplot(212)
plt.plot(data['Aggregate'].values,color='blue',label='original agregado')
plt.plot(data['washing machine'].values,color='black',label='original desagregado')
plt.legend()

#plt.subplot(313)
#plt.plot(corr,color='black',label='Correlação')
#plt.legend()
plt.show()
"""
