import pandas as pd
import matplotlib.pyplot as plt
import build_data as bd
from random import randint,shuffle,random
import nn
from keras.losses import logcosh
from keras.models import load_model
from keras.callbacks import EarlyStopping
import numpy as np
import os
from sklearn.preprocessing import normalize
import copy
import post_processing_regress as pp
from sklearn.metrics import f1_score
from datetime import datetime
import math
from keras import backend as K

os.environ["CUDA_VISIBLE_DEVICES"]="0"
dir_path = str(os.path.dirname(os.path.realpath(__file__)))

def mish(inputs):
        return inputs * K.tanh(K.softplus(inputs))

def get_tamMean(activations):


	m = 0
	for i in range(len(activations)):
		m+=len(activations[i])

	return m/len(activations)

def synthesize_aggregate(data,activations):
	original_aggregate = data['Aggregate']
	pointer = 0
	tam = len(original_aggregate)

	n = random()
	tw = (1-n)/n
	tw *= get_tamMean(activations)
	print("TW:",tw)

	artificial_aggregate = []
	while pointer < tam:
		r = int(np.random.exponential(scale=tw,size=1)[0])
		act = activations[randint(0,len(activations)-1)]

		if len(artificial_aggregate)+r > tam or len(artificial_aggregate)+r+len(act) > tam:
			artificial_aggregate.extend([0]*(tam - len(artificial_aggregate)))
			break
		else:
			artificial_aggregate.extend([0]*r)
			artificial_aggregate.extend(act)

		pointer+=r+len(act)

	data['Aggregate'] = [sum(x) for x in zip(original_aggregate, artificial_aggregate)]
	data['Desaggregate'] = [sum(x) for x in zip(data['Desaggregate'], artificial_aggregate)]

	return data

def get_activations(appliance,house):
	acts = []
	if appliance == 'kettle':
		a = bd.load_activations(appliance,2)
		acts.extend(a)		
	if appliance == 'dish washer':
		a = bd.load_activations(appliance,1)
		acts.extend(a)
		a = bd.load_activations(appliance,5)
		acts.extend(a)
		#a = bd.load_activations(appliance,2)
		#acts.extend(a)
	if appliance == 'washing machine':
		numActs = 4
	"""
	for i in range(numActs):	
		with open(dir_path+'/data/acts_'+appliance+'_House'+str(house)+'/'+str(i)+'.txt','r') as arquivo:
		#with open(dir_path+'/data/House'+str(house)+'/acts/acts_'+appliance+'_House'+str(house)+'/'+str(i)+'.txt','r') as arquivo:
			act = []
			for line in arquivo:
				currentPlace = line[:-1]
				act.append(float(currentPlace))
			acts.append(act)
	"""
	return acts

def des_normalize(data,minValue,maxValue):

	for key in data:
	
		time_serie = data[key]

		for i in range(len(time_serie)):
			time_serie[i] = time_serie[i]*(maxValue-minValue) + minValue

		data[key] = time_serie

	return data

def normalize_data(dict_data):

	maxValue = max(max(dict_data['Aggregate']), max(dict_data['Desaggregate']))
	minValue = min(min(dict_data['Aggregate']), min(dict_data['Desaggregate']))
	

	for key in dict_data:
	
		time_serie = dict_data[key]

		for i in range(len(time_serie)):
			time_serie[i] = (time_serie[i]-minValue)/(maxValue-minValue)

		dict_data[key] = time_serie

	return dict_data,minValue,maxValue

def build_windows(data,window_size,treshold):

	windows_X,windows_Y = [],[]
	windows_Z = []

	for i in range(0,len(data["Desaggregate"]),int(window_size)):  # Building windows

		if len(data["Desaggregate"])-i < window_size:
			break

		temp_win_x = data['Desaggregate'][i:i+window_size] #Apenas para extrair os keypoints
		#temp_win_y = [0,0,0]
		temp_win_y = [0,0]
		inner_activation = False
		j = 0
		while j < len(temp_win_x)-1:
	
			if temp_win_x[j] > treshold and not inner_activation:

				inner_activation = True
				begin_activation = j
				consume_inner_act = temp_win_x[j]
				break
			j+=1
		if not inner_activation:
			begin_activation = 0
		j = len(temp_win_x)-1
		while j >= begin_activation:
			if inner_activation and temp_win_x[j] < treshold and temp_win_x[j-1] < treshold: #Esse segundo AND é para prevenir de uns outliers que existem no meio da ativação
				temp_win_y[0] = begin_activation/window_size
				#temp_win_y[2] = consume_inner_act
				temp_win_y[1] = j/window_size
				inner_activation = False
				break
			j-=1

		#Caso a ativação comece e não termine dentro da janela analisada no momento
		if inner_activation:
			temp_win_y[0] = begin_activation/window_size
			#temp_win_y[2] = consume_inner_act
			temp_win_y[1] = 1



		windows_Z.append(temp_win_x)
		windows_X.append(data['Aggregate'][i:i+window_size])
		windows_Y.append(temp_win_y)
		

	'''
	plt.subplot(321)
	plt.title(windows_Y[0])
	plt.plot(windows_X[0])
	plt.plot(windows_Z[0])
	plt.subplot(322)
	plt.title(windows_Y[3])
	plt.plot(windows_X[3])
	plt.plot(windows_Z[3])
	plt.subplot(323)
	plt.title(windows_Y[13])
	plt.plot(windows_X[13])
	plt.plot(windows_Z[13])
	plt.subplot(324)
	plt.title(windows_Y[7])
	plt.plot(windows_X[7])
	plt.plot(windows_Z[7])
	plt.subplot(325)
	plt.title(windows_Y[5])
	plt.plot(windows_X[5])
	plt.plot(windows_Z[5])
	plt.subplot(326)
	plt.title(windows_Y[15])
	plt.plot(windows_X[15])
	plt.plot(windows_Z[15])
	plt.show()
	exit()
	'''

	return np.array(windows_X).reshape(np.shape(windows_X)[0],np.shape(windows_X)[1],1),np.array(windows_Y),windows_Z

		

def build_windows_2(data,window_size,pass_):

        windows_X = []
        windows_Y = []

        for i in range(0,len(data["Aggregate"]),pass_):  # Building windows

            windows_X.append(data['Aggregate'][i:i+window_size])
            #windows_Y.append(data["Desaggregate"][i:i+window_size])
            windows_Y.append([ data["Desaggregate"][i+int(window_size/2)-1],data["Desaggregate"][i+int(window_size/2)],data["Desaggregate"][i+int(window_size/2)+1]])
            if i+window_size == len(data["Aggregate"]):
                break
                
        if np.shape(windows_Y)[0] > 0:
                return np.array(windows_X).reshape(np.shape(windows_X)[0],np.shape(windows_X)[1],1),np.array(windows_Y)
        #return np.array(windows_X),np.array(windows_Y)
        return windows_X,windows_Y

def buildConsumptionCurve(predict):

    curve = []

    rate = [1,1,1]

    curve.append(predict[0][0])
    curve.append(predict[0][1])
    curve.append(predict[0][2])

    for i in range(1,np.shape(predict)[0]-1):

        curve[i] = (curve[i] + rate[1]*predict[i][0])/2
        curve[i+1] = (curve[i+1]+rate[2]*predict[i][1])/2

        curve.append(predict[i][2])

    del curve[0]

    return curve

def get_timeSerie_inferred(data,model,appliance,minValue,maxValue):

	window_size = int(model.inputs[0].shape[1])
	start_point = 0
	end_point = int(1000000/window_size)
	cts = 0
	time_serie_inferr = []
	for k in range(int(1000000/window_size),len(data['Aggregate']), int(1000000/window_size)):
			end_point = k

			print(k,'of',len(data['Aggregate']))
			
			current_data = {}
			current_data['Aggregate'] = data['Aggregate'][start_point:end_point]
			current_data['Desaggregate'] = data['Desaggregate'][start_point:end_point]
			#current_data['Timetable'] = data['Timetable'][start_point:end_point]
			
			
			testX,testY = build_windows_2(current_data,window_size,pass_ = 1) #Build windows to train neural network.

			if np.shape(testX)[0] > 0:
				predict = model.predict(testX,batch_size=256)
				predict = buildConsumptionCurve(predict)
				time_serie_inferr.extend(predict)
				del predict

			print("cts:",cts)
			
			cts += 1
			if cts == 183612:
				break

			print("Diff:",end_point-start_point)
			start_point = end_point-window_size+1
			

			del current_data

	data['Desaggregate'] = data['Desaggregate'][int(window_size/2):end_point-int(window_size/2)]
	data['Aggregate'] = data['Aggregate'][int(window_size/2):end_point-int(window_size/2)]

	for i in range(len(time_serie_inferr)):
		if time_serie_inferr[i] < (700 - minValue)/(maxValue-minValue):
			time_serie_inferr[i] = 0

	#plt.subplot(211)
	#plt.plot(time_serie_inferr,label='Consumo inferido antes',color='blue')
	#plt.plot(data['Desaggregate'],'--',label='real',color='red')
	#plt.legend()
	
	#plt.subplot(212)
	#time_serie_inferr = pp.use(time_serie_inferr,appliance)

	plt.plot(time_serie_inferr,label='Consumo inferido depois',color='red')
	plt.plot(data['Desaggregate'],'--',label='real')
	plt.legend()
	new_t = len(time_serie_inferr)

	data['Desaggregate'] = data['Desaggregate'][:new_t]
	data['Aggregate'] = data['Aggregate'][:new_t]
	data['Inferred'] = time_serie_inferr
	#plt.show()
	return data

def get_data_AMPds(appliance):

	dir_path = str(os.path.dirname(os.path.realpath(__file__)))

	location = dir_path+'/data/AMPds'
	data = pd.read_csv(location+'/Electricity_WHE.csv')[['unix_ts','P']]
	data['Aggregate'] = data.pop('P')

	#Convert timestamp to date (str)
	data['unix_ts'] = list(map(lambda x: datetime.utcfromtimestamp(int(x)).strftime('%Y-%m-%d %H:%M:%S'),data['unix_ts'].values))
	data["Time"] = data.pop('unix_ts') #Rename unix_ts to Time

	data[appliance] = pd.read_csv(location+'/Electricity_'+appliance+'.csv')['P']
	data["Desaggregate"] = data.pop(appliance)

	#Training period starts at 18 August 2012 and ends at 13 April 2013;
	#30 days were used as test sample (17 May 2013-17 June 2013).
	for i in range(len(data['Time'])):
		
		if data['Time'][i][:10] == '2012-08-17':
			b_train = i+1
		
		if data['Time'][i][:10] == '2013-04-12':
			e_train = i+1
		
		if data['Time'][i][:10] == '2013-05-16':
			b_test = i+1
		
		if data['Time'][i][:10] == '2013-06-16':
			e_test = i+1
			break
		
	data = data[['Aggregate',"Desaggregate"]]

	data_train = data.iloc[:e_train]
	data_test = data.iloc[b_test:e_test]

	data_test = {'Aggregate':list(data_test['Aggregate'].values),'Desaggregate':list(data_test['Desaggregate'].values)}
	data_train = {'Aggregate':list(data_train['Aggregate'].values),'Desaggregate':list(data_train['Desaggregate'].values)}

	return data_train,data_test

def get_data_UKDALE():
	data_list = []
	#A primeira ideia é treinar com todos os dados com exceção da casa 2
	for i in [5,4,1]:
		aux = bd.get_data_andMain(appliance,i,False)
		aux["Desaggregate"] = aux.pop(appliance)
		#if i == 1:
		#	aux['Desaggregate'] = aux['Desaggregate'][:10000000]
		#	aux['Aggregate'] = aux['Aggregate'][:10000000]	
		data_list.append(aux)
	
	#Extend e unify data
	data = {'Aggregate':[],'Desaggregate':[],'Time':[]}
	for i in data_list:
		data['Aggregate'].extend(i['Aggregate'])
		data['Desaggregate'].extend(i['Desaggregate'])
		#data['Time'].extend(i['Time'])
	
	del data_list

def get_samples(appliance):
	window_size = 100
	
	data,dataTest = get_data_AMPds(appliance)

	#data['Aggregate'] = data['Aggregate'][:30000] #DEBUG
	#data['Desaggregate'] = data['Desaggregate'][:30000] #DEBUG

	#Sintetização para aumentar o número de ativações no sinal
	#data = synthesize_aggregate(data,get_activations(appliance,house=2))

	data,minValue,maxValue = normalize_data(data)
	dataTest,minValue,maxValue = normalize_data(dataTest)

	#model = load_model(dir_path+'/models/best_model_'+appliance+'_Early_woSyn.h5')
	#data = get_timeSerie_inferred(data,model,appliance,minValue,maxValue)

	
	#O tamanho da janela será o tamanho médio das ativações + 50, idealmente deveria ser +(desvio padrao), dps arrumo isso
	#x_train,y_train = build_windows(data,50+int(get_tamMean(get_activations(appliance,house))))
	x_train,y_train,z = build_windows(data,window_size=window_size,treshold=(500-minValue)/(maxValue-minValue))
	x_test,y_test,_ = build_windows(dataTest,window_size=window_size,treshold=(500-minValue)/(maxValue-minValue))
	'''
	plt.plot(data['Desaggregate'])
	plt.show()
	
	plt.subplot(321)
	plt.title(y_train[2*648+1])
	plt.plot(x_train[2*648+1])
	plt.plot(z[648])
	plt.plot(int(y_train[2*648+1][0]*window_size),0.1,'ro')
	plt.plot(int(y_train[2*648+1][1]*window_size),0.1,'ro')
	
	plt.subplot(322)
	plt.title(y_train[2*649+1])
	plt.plot(x_train[2*649+1])
	plt.plot(z[2*649+1])
	plt.plot(int(y_train[2*649+1][0]*window_size),0.1,'ro')
	plt.plot(int(y_train[2*649+1][1]*window_size),0.1,'ro')
	
	plt.subplot(323)
	plt.title(y_train[2*943+1])
	plt.plot(x_train[2*943+1])
	plt.plot(z[2*943+1])
	plt.plot(int(y_train[2*943+1][0]*window_size),0.1,'ro')
	plt.plot(int(y_train[2*943+1][1]*window_size),0.1,'ro')
	
	plt.subplot(324)
	plt.title(y_train[2*944+1])
	plt.plot(x_train[2*944+1])
	plt.plot(z[2*944+1])
	plt.plot(int(y_train[2*944+1][0]*window_size),0.1,'ro')
	plt.plot(int(y_train[2*944+1][1]*window_size),0.1,'ro')
	
	
	plt.subplot(325)
	plt.title(y_train[130])
	plt.plot(x_train[130])
	plt.plot(z[130])
	plt.plot(int(y_train[130][0]*window_size),0.1,'ro')
	plt.plot(int(y_train[130][1]*window_size),0.1,'ro')
	
	
	plt.subplot(326)
	plt.title(y_train[202])
	plt.plot(x_train[202])
	plt.plot(z[202])
	plt.plot(int(y_train[202][0]*window_size),0.1,'ro')
	plt.plot(int(y_train[202][1]*window_size),0.1,'ro')
	
	#plt.show()
	#exit()
	'''

	return x_train,y_train,x_test,y_test

def train(appliance):
	
	x_train,y_train,x_test,y_test = get_samples(appliance)

	print("x_train shape:", np.shape(x_train))
	print("y_train shape:", np.shape(y_train))
	
	model = nn.build_model_PostProcessing(inputSize=len(x_train[0]),arch=5)
	#model = nn.build_model_PostProcessing(inputSize=200,arch=4)
	print(model.summary())
	#exit()
	
	model.compile(loss='mse',optimizer='adam')
	#model.compile(loss=logcosh,optimizer='adam')
	es = EarlyStopping(monitor='val_loss', mode='min', verbose=2,patience=500)

	
	#history = model.fit(x_train, y_train,verbose=1,batch_size=128,epochs=300,validation_split=0.2,callbacks=[es])
	history = model.fit(x_train, y_train,verbose=1,batch_size=128,epochs=3000,callbacks=[es],validation_data=(x_test,y_test))
	model.save('2postProcessing_model_regress_'+appliance+'_AMPds.h5')

	'''
	plt.plot(history.history['loss'])
	plt.plot(history.history['val_loss'])
	plt.title('model loss')
	plt.ylabel('loss')
	plt.xlabel('epoch')
	plt.legend(['train', 'test'], loc='upper left')
	#plt.show()
	'''
def evaluate(appliance):
	

	_,data = get_data_AMPds(appliance)

	'''
	data = bd.get_data_andMain(appliance,2)
	data["Desaggregate"] = data.pop(appliance)
	
	data['Aggregate'] = data['Aggregate'][1936259:]
	data['Desaggregate'] = data['Desaggregate'][1936259:]
	'''
	data,minValue,maxValue = normalize_data(data)

	#model = load_model(dir_path+'/models/best_model_'+appliance+'_Early_woSyn.h5')
	model = load_model(dir_path+'/best_model_'+appliance+'_AMPds_Mish.h5',custom_objects={'mish':mish})
	
	data = get_timeSerie_inferred(data,model,appliance,minValue,maxValue)

	#O tamanho da janela será o tamanho médio das ativações + 50, idealmente deveria ser +(desvio padrao), dps arrumo isso
	#x_train,y_train = build_windows(data,50+int(get_tamMean(get_activations(appliance,house))))
	#model = load_model('2postProcessing_model_regress_'+appliance+'_AMPds.h5',custom_objects={'mish':mish})
	model = load_model('2postProcessing_model_best_model_'+appliance+'_.h5',custom_objects={'mish':mish})
	window_size = int(model.inputs[0].shape[1])

	x_train,y_train,z = build_windows(data,window_size=window_size,treshold=(500-minValue)/(maxValue-minValue))


	predict = model.predict(x_train)


	print(np.shape(predict))
	print(np.shape(x_train))
	print(np.shape(y_train))
	'''
	plt.subplot(321)
	plt.title(predict[190])
	plt.plot(x_train[190])
	plt.plot(z[190])
	plt.plot(int(predict[190][0]*window_size),0.1,'ro')
	plt.plot(int(predict[190][1]*window_size),0.1,'ro')
	
	plt.subplot(322)
	plt.title(predict[212])
	plt.plot(x_train[212])
	plt.plot(z[212])
	plt.plot(int(predict[212][0]*window_size),0.1,'ro')
	plt.plot(int(predict[212][1]*window_size),0.1,'ro')
	
	plt.subplot(323)
	plt.title(predict[281])
	plt.plot(x_train[281])
	plt.plot(z[281])
	plt.plot(int(predict[281][0]*window_size),0.1,'ro')
	plt.plot(int(predict[281][1]*window_size),0.1,'ro')
	
	plt.subplot(324)
	plt.title(predict[429])
	plt.plot(x_train[429])
	plt.plot(z[429])
	plt.plot(int(predict[429][0]*window_size),0.1,'ro')
	plt.plot(int(predict[429][1]*window_size),0.1,'ro')
	
	
	plt.subplot(325)
	plt.title(predict[67])
	plt.plot(x_train[67])
	plt.plot(z[67])
	plt.plot(int(predict[67][0]*window_size),0.1,'ro')
	plt.plot(int(predict[67][1]*window_size),0.1,'ro')
	
	
	plt.subplot(326)
	plt.title(predict[68])
	plt.plot(x_train[68])
	plt.plot(z[68])
	plt.plot(int(predict[68][0]*window_size),0.1,'ro')
	plt.plot(int(predict[68][1]*window_size),0.1,'ro')
	
	plt.show()
'''
	plt.subplot(321)
	plt.title(predict[100])
	plt.plot(x_train[100])
	plt.plot(z[100])
	plt.plot(int(predict[100][0]*window_size),0.54,'ro')
	plt.plot(int(predict[100][1]*window_size),0.54,'ro')
	
	plt.subplot(322)
	plt.title(predict[99])
	plt.plot(x_train[99])
	plt.plot(z[99])
	plt.plot(int(predict[99][0]*window_size),0.54,'ro')
	plt.plot(int(predict[99][1]*window_size),0.54,'ro')
	
	plt.subplot(323)
	plt.title(predict[120])
	plt.plot(x_train[120])
	plt.plot(z[120])
	plt.plot(int(predict[120][0]*window_size),0.54,'ro')
	plt.plot(int(predict[120][1]*window_size),0.54,'ro')
	
	plt.subplot(324)
	plt.title(predict[55])
	plt.plot(x_train[55])
	plt.plot(z[55])
	plt.plot(int(predict[55][0]*window_size),0.54,'ro')
	plt.plot(int(predict[55][1]*window_size),0.54,'ro')
	
	
	plt.subplot(325)
	plt.title(predict[67])
	plt.plot(x_train[67])
	plt.plot(z[67])
	plt.plot(int(predict[67][0]*window_size),0.54,'ro')
	plt.plot(int(predict[67][1]*window_size),0.54,'ro')
	
	
	plt.subplot(326)
	plt.title(predict[68])
	plt.plot(x_train[68])
	plt.plot(z[68])
	plt.plot(int(predict[68][0]*window_size),0.54,'ro')
	plt.plot(int(predict[68][1]*window_size),0.54,'ro')
	
	plt.show()

	desagre = build_signal(predict,x_train,window_size)

	data['Aggregate'] = data['Aggregate'][:len(desagre)]
	data['Desaggregate'] = data['Desaggregate'][:len(desagre)]
	data['Inferred'] = data['Inferred'][:len(desagre)]

	plt.subplot(311)
	plt.plot(data['Aggregate'],label='Aggregate')
	plt.plot(data['Desaggregate'],label='Desaggregate')
	plt.legend()
	plt.subplot(312)
	plt.plot(desagre,label='Predict')
	plt.plot(data['Inferred'],label='Inferido')
	plt.plot(data['Desaggregate'],label='Desaggregate')
	plt.legend()
	plt.subplot(313)
	plt.plot(desagre,label='Predict')
	plt.plot(data['Aggregate'],label='Aggregate')
	plt.legend()
	plt.show()

	print(len(desagre),len(data['Aggregate']),len(data['Inferred']),len(data['Desaggregate']))
	ans = unify_solutions(data['Inferred'],desagre)

	plt.plot(desagre,label='Predict')
	plt.plot(data['Inferred'],label='Inferido')
	plt.plot(data['Desaggregate'],label='Desaggregate')
	plt.plot(ans,label='Unificado')
	plt.legend()
	plt.show()

	data['ans'] = ans

	data = des_normalize(data,minValue,maxValue)

	mae = 0

	for i in range(min(len(data['ans']),len(data['Desaggregate']))):
		mae += abs(data['Desaggregate'][i]-data['ans'][i])

	mae = mae/len(data['ans'])

	sae = abs(sum(data['Desaggregate']) - sum(data['ans']))/sum(data['Desaggregate'])

	dif = 0
	den = 0
	for i in range(np.shape(data['ans'])[0]):
		#if data['ans'][i][0] < 200:
		#	data['ans'][i][0] = 0.8
		dif += abs(data['ans'][i] - data['Desaggregate'][i])
		den+=data['Desaggregate'][i]

	ec =  1 - dif/(2*den)


	y_hat_label = []
	y_label = []
	for i in range(np.shape(data['ans'])[0]):
		if data['ans'][i] > 100:
			y_hat_label.append(1)
		else:
			y_hat_label.append(0)

		if data['Desaggregate'][i] > 100:
			y_label.append(1)
		else:
			y_label.append(0)

	f1 =  f1_score(y_label,y_hat_label)

	rmse = 0

	for i in range(min(len(data['ans']),len(data['Desaggregate']))):
		rmse += (data['Desaggregate'][i]-data['ans'][i])**2

	rmse = math.sqrt(rmse/len(data['ans']))
	nrmse = rmse/(max(data['Desaggregate'])-min(data['Desaggregate']))

	print('MAE:',mae,'SAE:',sae,'EC:',ec,'F1:',f1)
	print('RMSE:',rmse,'NRMSE',nrmse)
	

def unify_solutions(regression,points):

	ans = []
	for i in range(len(regression)):
		if points[i] > 0 and regression[i] > 0:
			ans.append(points[i])
		else:
			ans.append(regression[i])

	return ans



def build_signal(predict,x_train,window_size):

	desg = []

	for i in range(len(x_train)):
		aux = [0]*window_size
		if int(predict[i][0]*window_size) < 0:
			predict[i][0] = 0
		if int(predict[i][1]*window_size) < 0:
			predict[i][1] = 0
		if int(predict[i][1]*window_size) > window_size:
			predict[i][1] = 1
		aux[int(predict[i][0]*window_size):int(predict[i][1]*window_size)] = [0.551]*(int(predict[i][1]*window_size) - int(predict[i][0]*window_size))
		#Dish washer = 0.369
		#Kettle 0.54
		#DW_AMPds = 0.09
		if len(aux) != window_size:
			print('ausdhausd asdajsdasd PROBLEMMMMMM')
			print(len(aux))
			print((int(predict[i][1]*window_size) - int(predict[i][0]*window_size)))
			print(int(predict[i][0]*window_size),int(predict[i][1]*window_size))
		desg.extend(aux)


	return desg

		

if __name__ == '__main__':
	#train(appliance='DWE')
	evaluate('CDE')







