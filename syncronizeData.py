import pandas as pd
from datetime import datetime,timedelta
import os



def syncronee(localPrefixx,app_name):

    location = localPrefixx+'/Aggregate.csv'
    mainPower = pd.read_csv(location,sep=',')

    location = localPrefixx+'/'+app_name+'.csv'
    appliance = pd.read_csv(location,sep=',',names=['Time','Power'])

    startAggregate = 0

    oneSecond = timedelta(seconds=1)


    filteredMainPower = []
    filteredApliancePower = []
    houseFilteredTime = []


    month = int(datetime.strptime(appliance['Time'][2][:19], "%Y-%m-%d %H:%M:%S").month)

    print(len(mainPower['Time']))

    for i in range(2,len(appliance['Power'])):
        dateAppliance = datetime.strptime(appliance['Time'][i][:19], "%Y-%m-%d %H:%M:%S")
        for j in range(startAggregate,len(mainPower['Time'])):
            dateMainPower = datetime.strptime(mainPower['Time'][j][:19], "%Y-%m-%d %H:%M:%S")
            if dateAppliance == dateMainPower:
                filteredMainPower.append(mainPower['Power apparent'][j])
                filteredApliancePower.append(appliance['Power'][i])
                houseFilteredTime.append(mainPower['Time'][j][:19])
                startAggregate = j
                if int(mainPower['Time'][j][5:7]) == month:
                    print("Mês:" + str(month))
                    month = (month+1) % 13
                    if month == 0:
                        month +=1
                break
            elif dateAppliance == dateMainPower+oneSecond:
                filteredMainPower.append(mainPower['Power apparent'][j])
                filteredApliancePower.append(appliance['Power'][i])
                houseFilteredTime.append(mainPower['Time'][j][:19])
                startAggregate = j
                if int(mainPower['Time'][j][5:7]) == month:
                    print("Mês:" + str(month))
                    month = (month+1) % 13
                    if month == 0:
                        month +=1
                break
            elif dateAppliance == dateMainPower-oneSecond:
                filteredMainPower.append(mainPower['Power apparent'][j])
                filteredApliancePower.append(appliance['Power'][i])
                houseFilteredTime.append(mainPower['Time'][j][:19])
                startAggregate = j
                if int(mainPower['Time'][j][5:7]) == month:
                    print("Mês:" + str(month))
                    month = (month+1) % 13
                    if month == 0:
                        month +=1
                break
            if dateAppliance < dateMainPower:
                break


    print("mainPowerFiltered size: " + str(len(filteredMainPower)))
    print("APPLIANCE size: " + str(len(filteredApliancePower)))

    dfMain = pd.DataFrame(filteredMainPower)
    dfFridge = pd.DataFrame(filteredApliancePower)
    dfTime = pd.DataFrame(houseFilteredTime)

    location = localPrefixx+'/'+app_name+'AndMainComplete.csv'
    result = pd.concat([dfTime,dfMain,dfFridge],ignore_index=True,axis=1)
    result.to_csv(location,header=['Time','Aggregate','Desaggregate'],index=False)

    del dfMain
    del dfFridge
    del dfTime
    del result
    del mainPower
    del appliance

dir_path = str(os.path.dirname(os.path.realpath(__file__)))

app_names = ['broadband router','computer','external hard disk','laptop computer','toaster']


count = 1
for app_name in app_names:
    print(count,"of",len(app_names), app_name)
    syncronee(localPrefixx=dir_path+'/data/ukdale/House1',app_name=app_name)
    count+=1
