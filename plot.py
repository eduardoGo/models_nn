import os
import matplotlib.pyplot as plt
import pandas as pd
import re
import numpy as np

#plt.style.use('ggplot')
dir_path = str(os.path.dirname(os.path.realpath(__file__)))

def plot_dishWasher():

	f1 = [0.6857871331985741, 0.721496801232277, 0.7062123162162749, 0.7118462867325929, 0.7365169324060954, 0.7515553092850917,0.6922616162885058, 0.7167825538014224]
	ea = [0.0035120874051832287, -0.08506115410485031, -0.06357689982555881, 0.007451875721266021, -0.048485469502520286, 0.06668666777022136,-0.02457858773491488, -0.06008012722316749]
	

	f1_1 = [0.6553131091540016, 0.7258154104751819,0.7287260400672844, 0.7390072806549883, 0.7365169324060954, 0.7515553092850917,0.6922616162885058, 0.7167825538014224]
	ea_1 = [0.0748528185531357, -0.016017669997444143,-0.0027835527932426867, -0.00025527148846587653, -0.048485469502520286, 0.06668666777022136,-0.02457858773491488, -0.06008012722316749]

	f1_std = [0.05924904782847832, 0.035327442145132434, 0.06159358815092269, 0.05224561617738069, 0.032279832704965296,0.034356882528217926,0.033839755527960855, 0.05263762880321933]
	ea_std = [0.167707500660126, 0.19694833496924577, 0.11560045872234291, 0.07734536528048526, 0.21378719144760594,0.03362946385715522,0.08777683976363136, 0.1711527983368169]

	f1_std_1 = [0.07733239449129484, 0.07172044123108468,0.03047996749460714, 0.06526811036150526, 0.032279832704965296,0.034356882528217926,0.033839755527960855, 0.05263762880321933]
	ea_std_1 = [0.16115397937486983, 0.15941080093989085,0.1289098656871668, 0.1479896796292578, 0.21378719144760594,0.03362946385715522,0.08777683976363136, 0.1711527983368169]

	rmse = [847.0648617747149, 740.243828615176, 765.3900138256477, 748.7474535608006, 756.9435872961606, 738.9610359162649]
	nrmse = [0.07446422788052678, 0.06507374774780142, 0.06728431195644462, 0.06582128892715676, 0.06654179900584237, 0.06496097932572917]
	mae = [368.27127777572696, 347.4589176066468, 346.26276632537395, 328.53277917193725, 352.93659770105927, 319.5949048887393]
	sae = [0.8891873411053364, 0.8684918764198415, 1.394895946606797, 1.422317528675194, 1.4291379363455614, 1.5109351884485451]
	w = [100,200,300,400,500,600,700,800]

	plt.subplot(211)
	plt.title('Acurácia Estimada para diferentes tamanhos de janela deslizante (dish washer)')
	plt.errorbar(w, ea, ea_std, linestyle='--', fmt='-o',uplims=True, lolims=True,label='5 amostras')
	plt.errorbar(w, ea_1, ea_std_1, linestyle='--', fmt='^',uplims=True, lolims=True,label='30 amostras')
	plt.ylabel('EA')
	plt.xlabel('Tamanho da janela')
	plt.legend()
	plt.subplot(212)
	plt.title('F1 score (threshold = 2300) para diferentes \ntamanhos de janela deslizante (dish washer)')
	plt.errorbar(w, f1, f1_std, linestyle='--', fmt='-o',uplims=True, lolims=True,label='5 amostras')
	plt.errorbar(w, f1_1, f1_std_1, linestyle='--', fmt='^',uplims=True, lolims=True,label='30 amostras')
	plt.ylabel('F1')
	plt.xlabel('Tamanho da janela')
	plt.legend()
	plt.show()

def plot_fridge():

	f1 = [0.7609042300957913, 0.7292627611288924, 0.5851682220973886, 0.6731739981052669, 0.5821225388180261]
	ea = [0.7161111965317488, 0.7056327661178317, 0.6878811407644859, 0.6720669684742179, 0.672781431291827]
	f1_std = [0.03243916116632004, 0.05552646140045058, 0.2794377004061127, 0.0320850574635434, 0.07895969440604936]
	ea_std = [0.013266399075616886, 0.030617513171209032, 0.04095338469272053, 0.00940946057106466, 0.02480456514112154]

	rmse = [125.7202080439226, 126.42383878045158, 132.15874609704505, 136.31068725158252, 138.49058094427477]
	nrmse = [0.19297865889685123, 0.19405872166485527, 0.2028617195287094, 0.20923488775908142, 0.2125809923185528]
	mae = [91.77533058868659, 95.166952428972, 100.8600763298086, 106.07264171101174, 105.82080354299433]
	sae = [0.3021361514533464, 0.3903845732419634, 0.30200017231012055, 0.14838530991231175, 0.3329154205969609]
	
	w = [100,200,300,400,500]
	plt.subplot(211)
	plt.title('Acurácia Estimada para diferentes tamanhos de janela deslizante (fridge)')
	plt.errorbar(w, ea, ea_std, linestyle='--', fmt='-o',uplims=True, lolims=True)
	#plt.plot(w,ea,'--o')
	plt.ylabel('EA')
	plt.xlabel('Tamanho da janela')
	plt.legend()
	plt.subplot(212)
	plt.title('F1 score (threshold = 175) para diferentes \ntamanhos de janela deslizante (fridge)')
	plt.errorbar(w, f1, f1_std, linestyle='--', fmt='-o',uplims=True, lolims=True)
	plt.ylabel('F1')
	plt.xlabel('Tamanho da janela')
	plt.legend()
	plt.show()


def plot_Article():
	# Um gráfico com a renda média por faixa etária será nosso Hello World, vamos definir alguns valores fictícios
	app = ['DW', 'WM', 'WD']
	
	std_values_ea = [0.0229,0.0342,0.01249]
	std_values_f1 = [0.0286,0.08253,0.045]
	

	ea = [0.889, 0.7748, 0.64] #DW, WM, WD
	f1 = [0.959, 0.837, 0.648] #DW, WM, WD
	


	# Definindo a largura das barras
	barWidth = 0.5

	# Definindo a posição das barras
	r1 = np.arange(len(ea))
	#r2 = [x + barWidth for x in r1]
	# Criando as barras
	#plt.bar(r1, ea, color='#00DB39', width=barWidth, label='EA',yerr=std_values_ea)
	plt.bar(r1, f1, color='#6E1ADB', width=barWidth, label='f1-score',yerr=std_values_f1)
	
	# Adiciando legendas as barras
	plt.xlabel('Appliances',fontsize=20)
	plt.xticks([r for r in range(len(ea))], app,fontsize=20)
	plt.yticks([0.0,0.2,0.4,0.6,0.8,1],[0.0,0.2,0.4,0.6,0.8,1],fontsize=20)
	#plt.title('EA Average With Standard Deviation',fontsize=20)
	
	# Criando a legenda e exibindo o gráfico
	#plt.legend(fontsize=20,loc='upper right')
	
	plt.show()

plot_Article()