import nn as nn
import build_data as bd
#import outliers_data as ot
import data_sampling as ds
import math
from keras.optimizers import Adam,SGD
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from Early_Stopping import Early_Stopping
from keras.models import load_model
from keras.callbacks import LearningRateScheduler,ReduceLROnPlateau,ModelCheckpoint,EarlyStopping
import os
import copy
import time
from keras import backend as K
import tensorflow as tf

#export CUDA_VISIBLE_DEVICES=''
os.environ["CUDA_VISIBLE_DEVICES"]="1"
dir_path = str(os.path.dirname(os.path.realpath(__file__)))

def mish(inputs):
        return inputs * K.tanh(K.softplus(inputs))
def normalize_data(dict_data):
	
	#maxValue = max(max(dict_data['Aggregate']), max(dict_data['Desaggregate']))
	#minValue = min(min(dict_data['Aggregate']), min(dict_data['Desaggregate']))	

	for key in dict_data:
	
		time_serie = list(dict_data[key])
		
		maxValue = max(time_serie)
		minValue = min(time_serie)

		for i in range(len(time_serie)):
			time_serie[i] = (time_serie[i]-minValue)/(maxValue-minValue)

		dict_data[key] = time_serie

	return dict_data

def build_windows(data,window_size,pass_):

	windows_X = []
	windows_Y = []
	

	for i in range(0,int(len(data["Aggregate"])),pass_):  # Building windows
		
		if i+window_size > len(data["Aggregate"]):
			break
		
		windows_X.append(data['Aggregate'][i:i+window_size])
		#windows_Y.append(data["Desaggregate"][i:i+window_size])
		windows_Y.append([ data["Desaggregate"][i+int(window_size/2)-1],data["Desaggregate"][i+int(window_size/2)],data["Desaggregate"][i+int(window_size/2)+1]])


	return np.array(windows_X).reshape(np.shape(windows_X)[0],np.shape(windows_X)[1],1),np.array(windows_Y)


def train_nn(number,model,trainX,trainY,testX,testY,history,appliance,batch_size,epochs,early,times_reduction):

	window_size = int(model.inputs[0].shape[1])
	
	'''
	lower_lr = 0.0006
	upper_lr = 0.0057
	'''
	
	#scheduler_lr = scheduler_learningRate(lower_lr=0.0006,upper_lr=0.0057,interations_epoch = interations_epoch,max_epochs = epochs)
	
	#learn_rate = lr_schedule(0)
	#opt = Adam(lr=learn_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
	opt = Adam()
	model.compile(optimizer=opt, loss='mse')

	if early == None:
		early_stopping = Early_Stopping(max_epochs = epochs+1,min_loss = model.evaluate(np.array(testX), np.array(testY)))
	else:
		early_stopping = Early_Stopping(max_epochs = early,min_loss = model.evaluate(np.array(testX), np.array(testY)))

	#epochs=1
	for epoch in range(epochs):
		'''
		l = lr_schedule(epoch)
		if l != learn_rate:
			learn_rate = l
			opt = Adam(lr=learn_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
			model.compile(optimizer=opt, loss='mse')
	'''
		print("Epoch",epoch,"of",epochs)
		#print("Learning rate:",learn_rate)
		
		current_loss = model.fit(x=trainX,y=trainY,batch_size=batch_size,verbose=2,epochs=1)

		current_valLoss = model.test_on_batch(x = testX,y = testY)

		print('loss:',current_loss.history['loss'][0],'val_loss',current_valLoss)
		
		history['loss'].append(current_loss.history['loss'][0])
		
		history['val_loss'].append(current_valLoss)

		#history['learn_rate'].append(learn_rate)

		if early_stopping.count_loss(loss = current_valLoss,model = model, path_save = dir_path+'/models/best_model_'+appliance+'#'+str(number)+'_H2.h5'):
			print("Val_loss do not improved in {0} epochs".format(early_stopping.max_epochs))
			print("Epoch",epoch,"of",epochs)
			print("Stopping ...")
			break

		#model.save(dir_path+'/models/model_'+appliance+'_Wavelet_'+str(times_reduction)+'#'+str(number)+'_W'+str(window_size)+'.h5')

	
	return history,model



def main(number,model,times_reduction, window_size, appliance,house,batch_size,epochs,early,dataset='ukdale'):

	#window_size_reduced = int(WINDOW_SIZE/2**times_reduction)
	window_size_reduced = window_size

	'''
	Build or load model
	'''
	if model == None:
		model = nn.build_architect(window_size_reduced,features=1,arch=4) #Construct neural network without compile
	else:
		model = load_model(dir_path+'/models/best_model_'+appliance+'#'+str(number)+'.h5',custom_objects={'mish':mish})
	
	'''
	Get data. 
	It's return the dictionary that contais aggregate appliance and desaggregate data
	'''
	if dataset == 'ukdale':
		data = bd.get_data_andMain(appliance,house,sample1minute=True)
	elif dataset == 'AMPds':
		data = bd.get_data_AMPDS(appliance)
	else:
		print("Dataset not exist\nby: Dudu")
		exit()

	#Rename key, appliance to Desaggregate
	data["Desaggregate"] = data.pop(appliance)

	'''
	----------------------- CASA 5 ----------------------------------------------------------------

	TESTE - Washer Dryer --> Inicio, linha: 12915. Fim, linha: 34062

	TREINO - Washer Dryer --> Não tenho os dados do artigo, então vou usar o que sobrar depois de separar o teste.



	TESTE - Dish washer --> Inicio, linha: 12915. Fim, linha: 34062

	TREINO -Dish washer --> Não tenho os dados do artigo, então vou usar o que sobrar depois de separar o teste.


	----------------------- CASA 2 ----------------------------------------------------------------
	TESTE - Dish washer --> Inicio, linha: 175010. Fim, linha: final do arquivo

	TREINO - Dish washer --> Inicio, linha: 76081. Fim, linha: 100382
	
	[175010:]

	----------------------- CASA 1 ----------------------------------


	TESTE - Dish washer ---> Inicio, linha: 581891. Fim, linha: 706227

	TREINO - Dish washer ---> Inicio, linha: 16183987. Fim, linha: 1701997


	para washing machine, o segmento de teste é [563164:687357]

	'''
	if False:
	#if dataset == 'ukdale':
		dataTest = bd.get_data_andMain(appliance,house=1,sample1minute=True)
		dataTest["Desaggregate"] = dataTest.pop(appliance)
		
		#Split data for test
		dataTest['Aggregate'] = dataTest['Aggregate'][581891:706227]
		dataTest['Desaggregate'] = dataTest['Desaggregate'][581891:706227]
	else:
		dataTest = {}
		#Split data for test
		#tam = len(data['Aggregate'])
		dataTest['Aggregate'] = data['Aggregate'][175010:]
		dataTest['Desaggregate'] = data['Desaggregate'][175010:]
		
		del data['Aggregate'][175010:]
		del data['Desaggregate'][175010:]
		#Split data for train
		#data['Aggregate'] = data['Aggregate'][76081:100382]
		#data['Desaggregate'] = data['Desaggregate'][76081:100382]
		
		#Remove wrong data of the house 2
		#del data['Aggregate'][1080000:1600000]
		#del data['Desaggregate'][1080000:1600000]

	
	'''
	Reduced data to 1/2^times_reduction of the your 
	lenght originally utilizing wavelet transform. 
	'''
	print(len(dataTest['Aggregate']))
	print(len(dataTest['Desaggregate']))
	print(len(data['Aggregate']))
	print(len(data['Desaggregate']))

	dataTest['Aggregate'] = ds.reduce_data(dataTest['Aggregate'],times = times_reduction)
	dataTest['Desaggregate'] = ds.reduce_data(dataTest['Desaggregate'],times = times_reduction)
	
	data['Aggregate'] = ds.reduce_data(data['Aggregate'],times = times_reduction)
	data['Desaggregate'] = ds.reduce_data(data['Desaggregate'],times = times_reduction)
	

	#Eliminate the values < 0
	dataTest['Aggregate'] = list(map(lambda x: 0 if x < 0 else x,dataTest['Aggregate']))
	dataTest['Desaggregate'] = list(map(lambda x: 0 if x < 0 else x,dataTest['Desaggregate']))

	data['Aggregate'] = list(map(lambda x: 0 if x < 0 else x,data['Aggregate']))
	data['Desaggregate'] = list(map(lambda x: 0 if x < 0 else x,data['Desaggregate']))

	#Normalize the data to a range from 0 to 1
	data = normalize_data(data)
	dataTest = normalize_data(dataTest)

	'''
	Build windows for TEST and TRAIN.
	'''
	testX,testY = build_windows(dataTest,window_size_reduced,pass_ = 1) #Build windows to train neural network.
	trainX,trainY = build_windows(data,window_size_reduced,pass_ = 1) #Build windows to train neural network.

	print("Shape of testX:",np.shape(testX))
	print("Shape of testY:",np.shape(testY))
	print("Shape of trainX:",np.shape(trainX))
	print("Shape of trainY:",np.shape(trainY))

	history = {'loss':[],'val_loss':[]}

	'''
	Train model
	'''
	b = time.time()
	history,model = train_nn(number,model,trainX,trainY,testX,testY,history,appliance,batch_size,epochs,early,times_reduction)
	totalTime = time.time() - b
	pd.DataFrame(history).to_csv('history_'+appliance+'.csv')
	K.clear_session()
	tf.keras.backend.clear_session()

	del dataTest
	del data
	del trainX,trainY,testX,testY,history
	#pd.DataFrame(history).to_csv('history.csv')

	print("Tempo total gasto", totalTime/3600, ' hrs')

	return model

if __name__ == '__main__':
	'''
	apps = {
	'active speaker':[5],
	'broadband router':[1], 
	'computer':[5,1],
	'external hard disk':[1],
	'laptop computer':[1],
	'running machine':[5],
	'toaster':[5,1],
	'dish washer':[5,1],
	'fridge':[5,1],
	'kettle':[5,1],
	'microwave':[5,1],
	'washing machine':[4]}
	'''

	#apps = {
	#'dish washer':[5]}
	#'washing machine':[4]}
	#'washer dryer':[5]}

	apps = {
	#'DWE':[5],
	#'CDE':[1],
	#'WOE':[1]}
	#'washing machine':[4]}
	#'washer dryer':[5]}
	'fridge':[2],
	'kettle':[2],
	'microwave':[2]}

	b = time.time()
	for app in apps:
		for i in range(1):
			print('ROUND:',i)
			for house_ind in range(len(apps[app])):
				print("###################################")
				print("TREINAMENTO NA CASA", apps[app][house_ind],"INICIADO")
				print("###################################")
				for win_size in [400]:
					if house_ind == 0:
						m = main(i,None,times_reduction=1, window_size=win_size, appliance=app,house=apps[app][house_ind],batch_size=256,epochs=3000,early=250,dataset='ukdale')
					else:
						m = main(i,'na',times_reduction=1, window_size=win_size, appliance=app,house=apps[app][house_ind],batch_size=256,epochs=3000,early=250)
	totalTime = time.time() - b
	print("Tempo total gasto (busca completa):", totalTime/3600, ' hrs')
	
	

