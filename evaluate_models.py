import nn as nn
import build_data as bd
#import outliers_data as ot
import data_sampling as ds
import math
from keras.models import load_model
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import copy
from keras import backend as K
from sklearn.metrics import f1_score,classification_report,confusion_matrix
import scipy.stats as st
import my_postProcessing as pp

#WINDOW_SIZE = 701
#APPLIANCE = 'dish washer'

#plt.style.use('ggplot')
os.environ["CUDA_VISIBLE_DEVICES"]="0"
dir_path = str(os.path.dirname(os.path.realpath(__file__)))

def transform_solution(predict,desagg,window_size):
	return b2uild_windows(predict,window_size),b2uild_windows(desagg,window_size)

def b2uild_windows(time_serie,window_size):

	windows_Y = []
	

	for i in range(0,len(time_serie),window_size):  # Building windows
		windows_Y.append(sum(time_serie[i:i+window_size])/window_size)
	
	return windows_Y

def mish(inputs):
        return inputs * K.tanh(K.softplus(inputs))

def RMSE(y_pred,y_true):
	
	rmse = 0

	for i in range(min(len(y_pred),len(y_true))):
		rmse += (y_true[i]-y_pred[i])**2

	rmse = math.sqrt(rmse/len(y_pred))
	nrmse = rmse/(max(y_true)-min(y_true))
	return rmse,nrmse

def test_acc(y_hat,y,transform=True):
	window_size = 400

	if transform:
		y_hat,y= transform_solution(y_hat,y,window_size)
	
	dif = 0
	den = 0
	for i in range(np.shape(y_hat)[0]):
		#if y_hat[i][0] < 200:
		#	y_hat[i][0] = 0.8
		dif += abs(y_hat[i] - y[i])
		den+=y[i]

	return 1 - dif/(2*den)


def calc_f1(y_hat,y,transform,appliance):
	window_size = 400
	plt.clf()
	plt.plot(y,label='true consume')
	plt.plot(y_hat,label='infered consume')
	plt.legend()
	plt.ylim(0,400)
	plt.xlim(0,1000)
	#plt.savefig('kettle_notTransfrm.pdf')

	if appliance == 'microwave':
		thrsh = 50
		#plt.savefig('microwave_notTransfrm.pdf')
	elif appliance == 'fridge':
		thrsh = 40
		#plt.savefig('fridge_notTransfrm.pdf')
	elif appliance == 'dish washer':
		thrsh = 10
	elif appliance == 'kettle':
		thrsh = 10
		#plt.savefig('kettle_notTransfrm.pdf')
	elif appliance == 'washing machine':
		thrsh = 20
	else:
		print("Erro! thrsh não setado! Appliance:", appliance)

	#y_hat = [x-58 for x in y_hat]
	if transform:
		y_hat,y= transform_solution(y_hat,y,window_size)

	#plt.clf()
	#plt.plot(y,'--o',label='true consume')
	#plt.plot(y_hat,'--o',label='infered consume')
	#plt.legend()
	#plt.savefig('kettle_transformed.pdf')
	#exit()
	y_hat_label = []
	y_label = []
	for i in range(np.shape(y_hat)[0]):
		
		if y_hat[i] > thrsh:
			y_hat_label.append(1)
		else:
			y_hat_label.append(0)

		if y[i] > thrsh:
			y_label.append(1)
		else:
			y_label.append(0)
	tn, fp, fn, tp = confusion_matrix(y_label,y_hat_label).ravel()
	#print(aff)
	print(tp,fp,'\n',fn,tn)
	print(classification_report(y_label,y_hat_label))
	return f1_score(y_label,y_hat_label)

def metricsMAE(y_pred,y_true):
    
    mae = 0
    
    for i in range(min(len(y_true),len(y_pred))):
        mae += abs(y_true[i]-y_pred[i])

    return mae/len(y_pred)

def metricSAE(y_pred,y_true):

    return abs(sum(y_true) - sum(y_pred))/max(sum(y_true),sum(y_pred))

def metricsSAEdelt(y_pred,y_true):

	ns = 600
	saeDelt = []
	
	for i in range(ns,len(y_pred),ns):	
		rHat = sum(y_pred[i-ns:i])
		r = sum(y_true[i-ns:i])

		saeDelt.append(abs(r - rHat)/ns)

	return np.mean(saeDelt)


def des_normalize(time_serie,minValue,maxValue):

	#minValue = abs(minValue)
	for i in range(len(time_serie)):
		time_serie[i] = time_serie[i]*(maxValue-minValue) + minValue
		#if time_serie[i] < 0:
		#	time_serie[i] = 0


	return time_serie


def normalize_data(dict_data):

	values_normalize = {}

	#maxValue = max(max(dict_data['Aggregate']), max(dict_data['Desaggregate']))
	#minValue = min(min(dict_data['Aggregate']), min(dict_data['Desaggregate']))
	
	#print(maxValue,minValue)
	#exit()
	for key in dict_data:
	
		time_serie = list(dict_data[key])
		maxValue = max(time_serie)
		minValue = min(time_serie)
		
		values_normalize[key] = [maxValue,minValue]

		for i in range(len(time_serie)):
			
			time_serie[i] = (time_serie[i]-minValue)/(maxValue-minValue)
		
		dict_data[key] = time_serie

	return dict_data,values_normalize


def build_windows(data,window_size,pass_):

	windows_X = []
	windows_Y = []
	

	for i in range(0,len(data["Aggregate"]),pass_):  # Building windows
		
		if i+window_size > len(data["Aggregate"]):
			break
		
		'''
		auxList = []
		for j in range(window_size):
			auxList.append([data['Aggregate'][i+j],data['Timetable'][i+j]])
		'''
		windows_X.append(data['Aggregate'][i:i+window_size])
		#windows_Y.append(data["Desaggregate"][i:i+window_size])
		windows_Y.append([ data["Desaggregate"][i+int(window_size/2)-1],data["Desaggregate"][i+int(window_size/2)],data["Desaggregate"][i+int(window_size/2)+1]])

	print(np.shape(windows_Y))
	print(np.shape(windows_X))
	#exit()
	if np.shape(windows_Y)[0] > 0:
		return np.array(windows_X).reshape(np.shape(windows_X)[0],np.shape(windows_X)[1],1),np.array(windows_Y)
	#return np.array(windows_X),np.array(windows_Y)
	return windows_X,windows_Y

def build_timeSerie2(predict):
	ts = []
	qt_windows = np.shape(predict)[0]
	for i in range(qt_windows+2):
		if i == 0:
			ts.append(predict[i][0])
		elif i == 1:
			ts.append( (predict[i-1][1]+predict[i][0])/2 )
		elif i == qt_windows:
			ts.append( (predict[i-2][2]+predict[i-1][1])/2  )
		elif i == qt_windows+1:
			ts.append( predict[i-2][2]  )
		else:
			ts.append( (predict[i-2][2]+predict[i-1][1]+predict[i][0])/3  )
	
	del ts[0]
	del ts[-1]

	return ts
def build_timeSerie(predict):

	time_serie = list(predict[0])

	for i in range(1,np.shape(predict)[0]):

		time_serie.append(predict[i][-1])

		for k in range(i,len(time_serie)):
			time_serie[k] += predict[i][i-k]
			time_serie[k] /= 2

	return time_serie

def buildConsumptionCurve(predict):

    curve = []

    rate = [1,1,1]

    curve.append(predict[0][0])
    curve.append(predict[0][1])
    curve.append(predict[0][2])

    for i in range(1,np.shape(predict)[0]-1):

        curve[i] = (curve[i] + rate[1]*predict[i][0])/2
        curve[i+1] = (curve[i+1]+rate[2]*predict[i][1])/2

        curve.append(predict[i][2])

    del curve[0]
    del curve[-1]

    return curve


def median_filter(time_series,window_size):

	for i in range(int(window_size/2),len(time_series)-1-int(window_size/2)):
		time_series[i] = np.median(time_series[i-int(window_size/2):i+int(window_size)])

	return time_series

def evaluate(appliance,model,testX,data,values_normalize,window_size_reduced,batch_size,delay_window):

	predict = model.predict(testX,batch_size=batch_size)

	predict = buildConsumptionCurve(predict)
	#predict = build_timeSerie2(predict)
	#predict[0],predict[1],predict[-1],predict[-2] = predict[2],predict[2],predict[-3],predict[-4]

	#for l in range(len(predict)):
		#predict[l] += 0.18 + 0.0815
		#predict[l] -= 0.024

	#print('###1###',len(data['Desaggregate_wave']),len(predict))
	
	#Exclui a metade da primeira janela e a metade da última janela
	#Como a janela é utilizada no sinal amostrado, então no sinal original ela *times maior
	data['Desaggregate'] = data['Desaggregate'][int(window_size_reduced):- int(window_size_reduced)]
	
	data['Aggregate'] = data['Aggregate'][int(window_size_reduced/2):- int(window_size_reduced/2)]

	#plt.clf()
	#plt.plot(data['Desaggregate_wave'],label='true consume')
	#plt.plot(predict,label='infered consume')
	#plt.legend()
	#plt.savefig('microwave_notTransfrm_Normalized.pdf')
	
	predict = des_normalize(predict,values_normalize['Desaggregate_wave'][1],values_normalize['Desaggregate_wave'][0])
	data['Desaggregate'] = des_normalize(data['Desaggregate'],values_normalize['Desaggregate'][1],values_normalize['Desaggregate'][0])
	data['Aggregate'] = des_normalize(data['Aggregate'],values_normalize['Aggregate'][1],values_normalize['Aggregate'][0])

	#plt.subplot(311)
	#plt.title("Amostras da série temporal (Transformada de Wavelet)")
	#plt.plot(data['Aggregate'][int(32000/16):int(36000/16)],label='Aggregate',color='blue')
	#plt.plot(predict[int(32000/16):int(36000/16)],label='Desagregate',color='black')
	#plt.legend()

	data['Aggregate'] = ds.invert_dwt(data['Aggregate'],'db18',1)[:-1]
	predict = ds.invert_dwt(predict,'db18',1)[:-1]
	
	#for l in range(len(predict)):
	#	if predict[l] < 100:
	#		predict[l] = 0
	
	#	else:
	#		predict[l] *= 2.5

	#for l in range(len(predict)):
	#	predict[l] -=150

	#for l in range(len(predict)):
	#	if predict[l] < 10:
	#		predict[l] =10
		
	#data['Desaggregate'] = ds.invert_dwt(data['Desaggregate'],'db14',4)
	
	for i in range(len(predict)):
		if predict[i] < 0:
			predict[i] = 0

	print('real consume:',np.shape(data['Desaggregate']))
	print('predict:',np.shape(predict))
	
	mae = metricsMAE(y_pred=predict,y_true=data['Desaggregate'])
	sae = metricSAE(y_pred=predict,y_true=data['Desaggregate'])
	saeDelt = metricsSAEdelt(y_pred=predict,y_true=data['Desaggregate'])
	f1,ea = calc_f1(predict,data['Desaggregate'],True,appliance),test_acc(predict,data['Desaggregate'],True)
	
	print('F1 before:',f1)
	print('EA before:',ea)

	print('MAE before:',mae)
	print('SAE before:',sae)
	print('SAE delt: ',saeDelt)
	
	return mae,sae,0,0,f1,ea,predict,data['Aggregate']

	'''
	plt.show()
	#plt.subplot(211)
	plt.plot(data['Aggregate'],label='Consumo agregado',color='blue')
	plt.plot(data['Desaggregate'],label='Consumo individual',color='red')
	plt.plot(predict,label='Consumo desagregado',color='black')
	plt.legend(fontsize=20)
	plt.xticks([])
	plt.ylabel("Potência", fontsize=20)
	plt.show()
	plt.subplot(212)
	plt.plot(predict,label='Desagregate',color='black')
	plt.plot(data['Desaggregate'],label='Consumo real',color='red')
	plt.legend()
	mae = metricsMAE(y_pred=predict,y_true=data['Desaggregate'])
	sae = metricSAE(y_pred=predict,y_true=data['Desaggregate'])

	f1,ea = calc_f1(predict,data['Desaggregate'],True),test_acc(predict,data['Desaggregate'],True)
	
	print('F1 before:',f1)
	print('EA before:',ea)

	#print('MAE after:',mae)
	#print('SAE after:',sae)
	plt.show()
	print(len(data['Aggregate']),len(data['Desaggregate']),len(predict))
	plt.subplot(312)
	plt.title("Séries originais (Transformada inversa)")
	plt.plot(data['Aggregate'][32000:36000],label='Aggregate',color='blue')
	plt.plot(predict[32000:36000],label='Desagregate',color='black')
	plt.legend()
	plt.subplot(313)
	plt.title("Séries originais (Transformada inversa)")
	plt.plot(predict,label='Desagregate',color='black')
	plt.plot(data['Desaggregate'],label='Consumo real',color='red')
	plt.legend()
	plt.show()
	#exit()
	rmse,nrmse = RMSE(predict,data['Desaggregate'])

	print("RMSE:",rmse)
	print('NRMSE:',nrmse)
	'''
	#predict = b2uild_windows(predict,100)
	#data['Desaggregate'] = b2uild_windows(data['Desaggregate'],100)
	#plt.show()
	plt.plot(data['Desaggregate'], label='Consumo real')
	plt.plot(predict,'--',label='Before post processing')
	plt.savefig('teste.png')
	exit()
	#predict = pp.use(predict,appliance)
	data['Desaggregate'] = data['Desaggregate'][:len(predict)]
	
	f1 = calc_f1(predict,data['Desaggregate'],False)
	ea = test_acc(predict,data['Desaggregate'],False)
	print('F1 after:',f1)
	print('EA after:',ea)
	plt.plot(predict,'-o',label='After post processing')
	plt.legend()
	#plt.show()
	plt.savefig('teste.png')
	return mae,sae,0,0,f1,ea,predict,data['Aggregate']

	plt.title("Séries originais (Transformada inversa)")
	plt.plot(predict,label='Desagregate',color='black')
	plt.plot(data['Desaggregate'],label='Consumo real',color='red')
	plt.legend()
	plt.show()

	
	#plt.subplot(211)
	#plt.title("Rede neural do dishwasher treinada com sinal reduzido")
	if appliance == 'fridge':
		plt.plot(data['Aggregate'],label='Sinal agregado',color='black')
	#plt.plot(data['Desaggregate'],label='Sinal individual',color='red')
	#plt.legend(loc='upper right')
	#plt.xlabel("Quantidade de amostras")
	#plt.ylabel("Watts")
	#plt.subplot(212)
	#plt.plot(data['Desaggregate'],label='Sinal individual',color='red')
	#plt.plot(predict,label=appliance)
	#plt.legend(loc='upper right')
	#plt.xlabel("Quantidade de amostras")
	#plt.ylabel("Watts")	
	

	return mae,sae,rmse,nrmse,f1,ea,predict,data['Aggregate']

def main(model, window_size,appliance,times_reduction,dataset='ukdale'):
	#window_size_reduced = int(WINDOW_SIZE/2**TIMES_REDUCTION)
	window_size_reduced = window_size

	#model = load_model(dir_path+'/models/model_'+appliance+'.h5')
	#print(model.summary())

	
	if dataset == 'ukdale':
		data = bd.get_data_andMain(appliance,house=2,sample1minute=True)
		#dataTest['Aggregate'] = dataTest['Aggregate'][:int(len(dataTest['Aggregate'])*0.7)]
		#dataTest[appliance] = dataTest[appliance][:int(len(dataTest[appliance])*0.7)]
		dataTest = {}
		#tam = len(data['Aggregate'])
		dataTest['Aggregate'] = data['Aggregate'][175010:]
		dataTest[appliance] = data[appliance][175010:]

		del data['Aggregate'][175010:]
		del data[appliance][175010:]

	elif dataset == 'AMPds':
		dataTest = bd.get_data_AMPDS(appliance)
		tam = len(dataTest['Aggregate'])
		dataTest['Aggregate'] = dataTest['Aggregate'][:int(tam*0.2)]
		dataTest[appliance] = dataTest[appliance][:int(tam*0.2)]
	else:
		print("Dataset not exist\nby: Dudu")
		exit()

	dataTest["Desaggregate"] = dataTest.pop(appliance)
	#print(len(dataTest["Desaggregate"]))
	#print(sum(dataTest["Desaggregate"]))
	#plt.show()
	#plt.plot(dataTest["Desaggregate"])
	#plt.savefig('microwave.pdf')
	#exit()
	#tam=174967
	#tam = 1750092 #sample rate = 6s
	#tam = 174967 #sample rate = 1min

	#dataTest['Aggregate'] = dataTest['Aggregate'][563164:687357]
	#dataTest['Desaggregate'] = dataTest['Desaggregate'][563164:687357]


	#Reduced data to 1/2^TIMES_REDUCTION of the your lenght originally utilizing wavelet transform. 
	dataTest['Aggregate'] = ds.reduce_data(copy.copy(dataTest['Aggregate']),times = times_reduction)
	dataTest['Desaggregate_wave'] = ds.reduce_data(copy.copy(dataTest['Desaggregate']),times = times_reduction)

	#Eliminate the values < 0
	dataTest['Aggregate'] = list(map(lambda x: 0 if x < 0 else x,dataTest['Aggregate']))

	'''
	plt.subplot(212)
	plt.plot(dataTest['Aggregate'][:6250],label='agg',color='black')
	plt.plot((dataTest[appliance])[:6250],label='real',color='red')
	plt.legend()

	plt.show()

	exit()

	'''


	'''
	plt.subplot(311)
	plt.plot(dataTest['Aggregate'],label='Aggregate',color='blue')
	plt.plot(dataTest['Desaggregate'],label='desaggregate',color='black')
	plt.legend()
	'''


	dataTest,values_normalize = normalize_data(dataTest)

	#dataTest = eliminate_noise(dataTest)
	#dataTest['Aggregate'] = median_filter(dataTest['Aggregate'],window_size=5)
	#dataTest['Desaggregate'] = median_filter(dataTest['Desaggregate'],window_size=5)

	'''
	plt.subplot(312)
	plt.plot(dataTest['Desaggregate'],label='Disaggregate',color='blue')
	plt.legend()
	'''

	'''
	plt.subplot(313)
	plt.plot(dataTest['Desaggregate'],label='Disaggregate',color='blue')
	plt.legend()
	plt.show()
	'''

	#dataTest['Aggregate'] = dataTest['Aggregate'][200000:]
	#dataTest['Desaggregate'] = dataTest['Desaggregate'][200000:]
	#dataTest['Timetable'] = dataTest['Timetable'][200000:]

	'''
	plt.subplot(211)
	plt.plot(dataTest['Aggregate'][:100000],label='Aggregate',color='blue')
	plt.plot(dataTest['Desaggregate'][:100000],label='desaggregate',color='black')
	plt.legend()
	plt.subplot(212)
	plt.plot(dataTest['Desaggregate'][:100000],label='Aggregate',color='blue')
	plt.legend()
	plt.show()
	exit()
	'''

	testX,testY = build_windows(dataTest,window_size_reduced,pass_ = 1) #Build windows to train neural network.

	return evaluate(appliance,model,testX,dataTest,values_normalize,window_size_reduced,batch_size=256,delay_window=1)

if __name__ == '__main__':
	
	apps = ['active speaker',
	'broadband router', 
	'computer',
	'external hard disk',
	'laptop computer',
	'running machine',
	'toaster',
	'dish washer',
	'fridge',
	'kettle',
	'washing machine',
	'washer dryer']
	#'microwave',
	apps= [apps[8], apps[9],'microwave']
	#apps=['microwave','kettle']
	#apps= ['HPE']
	#apps= ['WOE']
	total_signal = [0]*5925
	#plt.subplot(211)
	for appliance in apps:
		#plt.title(appliance)
#	appliance = 'washing machine'
		print("----------------\nAPPLIANCE:",appliance,'\n-----------------------------')
		times_reduction = 1
		mae_list,sae_list,rmse_list,nrmse_list,f1_list,ea_list = [],[],[],[],[],[]
		mae_list_std,sae_list_std,rmse_list_std,nrmse_list_std,f1_list_std,ea_list_std = [],[],[],[],[],[]
		for window_size in [400]:
			qt = 10
			mae,sae,rmse,nrmse,f1,ea = [],[],[],[],[],[]
			for i in range(0,1):
				print('MODEL NUMBER',i)
				#model = load_model(dir_path+'/models/best_model_microwave_Early.h5')
				#model = load_model(dir_path+'/models/model_'+appliance+'_Wavelet_4.h5',custom_objects={'mish':mish})
				#model = load_model(dir_path+'/models/best_model_'+appliance+'_Wavelet_'+str(times_reduction)+'#'+str(i)+'_W'+str(window_size)+'_twoIndependent_House2.h5',custom_objects={'mish':mish})
				model = load_model(dir_path+'/models/best_model_'+appliance+'#'+str(i)+'_H2.h5',custom_objects={'mish':mish})
				a,b,c,d,f,g,signal,aggr = main(model, window_size=window_size,appliance=appliance,times_reduction=times_reduction)
				total_signal = [x+y for x,y in zip(total_signal,signal)]
				mae.append(a)
				sae.append(b)
				rmse.append(c)
				nrmse.append(d)
				f1.append(f)
				ea.append(g)

			'''
			print('F1:',np.mean(f1),'+-',np.std(f1))
			print('EA:',np.mean(ea),'+-',np.std(ea))
			print("RMSE:",np.mean(rmse),'+-',np.std(rmse))
			print('NRMSE:',np.mean(nrmse),'+-',np.std(nrmse))
			print('MAE:',np.mean(mae),'+-',np.std(mae))
			print('SAE:',np.mean(sae),'+-',np.std(sae))
			'''
			f1_list.append(np.mean(f1))
			ea_list.append(np.mean(ea))
			rmse_list.append(np.mean(rmse))
			nrmse_list.append(np.mean(nrmse))
			mae_list.append(np.mean(mae))
			sae_list.append(np.mean(sae))

			f1_list_std.append(np.std(f1))
			ea_list_std.append(np.std(ea))
			rmse_list_std.append(np.std(rmse))
			nrmse_list_std.append(np.std(nrmse))
			mae_list_std.append(np.std(mae))
			sae_list_std.append(np.std(sae))

		print(f1_list)
		print(f1_list_std)
		print(ea_list)
		print(ea_list_std)

	'''
	residuo = [x-y for x,y in zip(total_signal,aggr)]
	print(residuo)
	plt.legend(loc='upper right')
	plt.subplot(212)
	plt.plot(residuo,label='residuo',color='black')
	plt.legend()
	plt.show()
	'''
