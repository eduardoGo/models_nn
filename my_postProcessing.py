import pandas as pd
import matplotlib.pyplot as plt
import build_data as bd
from random import randint,shuffle,random
import nn
from keras.models import load_model
from keras.callbacks import EarlyStopping
import numpy as np
import os
from sklearn.preprocessing import normalize
import copy
import data_sampling as ds

os.environ["CUDA_VISIBLE_DEVICES"]="1"
dir_path = str(os.path.dirname(os.path.realpath(__file__)))

def find_next_window(current_point,timeSerie,treshold):

	while current_point+1 < len(timeSerie):
		if timeSerie[current_point] > treshold:
			return current_point
		else:
			current_point+=1

	return current_point

def build_windows(timeSerie,window_size):
	windows_X = []
	windows_Y = []
	'''
	point = 0
	#point = find_next_window(point,timeSerie,1000)
	
	mean_consume_porW = []
	while point+400 < len(timeSerie):
		mean_consume_porW.append(sum(timeSerie[point:point+400])/400)
		point += 400

	point = 0
	while point+window_size < len(mean_consume_porW):
		windows_X.append(mean_consume_porW[point:point+window_size])
		windows_Y.append(1)
		
		#point = find_next_window(point+window_size,timeSerie,1000)
		point += window_size

	'''

	for i in range(0,len(timeSerie),window_size):  # Building windows
		
		if i+window_size > len(timeSerie):
			break
		
		
		windows_X.append(timeSerie[i:i+window_size])
		windows_Y.append(1)

	print(np.shape(windows_Y))
	print(np.shape(windows_X))
	c = list(zip(windows_X,windows_Y))
	shuffle(c)
	windows_X,windows_Y = zip(*c)

	return windows_X,windows_Y


def get_activations(appliance):
	acts = []
	
	if appliance == 'kettle':
		a = bd.load_activations(appliance,2)
		acts.extend(a)		
	if appliance == 'dish washer':
		a = bd.load_activations(appliance,2)
		acts.extend(a)
		#a = bd.load_activations(appliance,5)
		#acts.extend(a)
		#a = bd.load_activations(appliance,2)
		#acts.extend(a)
	if appliance == 'washing machine':
		a = bd.load_activations(appliance,2)
		acts.extend(a)

	
	#Run wavelet transform
	for i in range(len(acts)):
		acts[i] = ds.reduce_data(acts[i],times = 4)

	#Run wavelet transform invert
	for i in range(len(acts)):
		acts[i] = ds.invert_dwt(acts[i],times=4)
	
	return acts


def get_tamMean(activations):


	m = 0
	for i in range(len(activations)):
		m+=len(activations[i])

	return m/len(activations)



def get_positiveSamples(numSamples,appliance):

	activations = get_activations(appliance)
	
	lMean = int(get_tamMean(activations))
	print("Len mean:",lMean)

	
	#W2P --> WINDOW TO POINT

	window_size = 50
	window_size_W2P = 100
	n = random()
	tw = (1-n)/n
	tw *= lMean
	print("TW:",tw)

	tam = numSamples*window_size
	print('tam:',tam)
	artificial_data = []
	pointer = 0
	while pointer < tam:
		r = int(np.random.exponential(scale=tw,size=1)[0])
		act = activations[randint(0,len(activations)-1)]

		if len(artificial_data)+r > tam or len(artificial_data)+r+len(act) > tam:
			artificial_data.extend([0]*(tam - len(artificial_data)))
			break
		else:
			artificial_data.extend([0]*r)
			artificial_data.extend(act)

		pointer+=r+len(act)


	plt.plot(artificial_data,label='artificial_data')
	plt.legend()
	plt.show()
	x_train,y_train = build_windows(artificial_data,window_size=window_size)
	
	return x_train,y_train


def get_negativeSamples(posSamples,numSamples):
	negSamples = []
	y = []
	window_size = len(posSamples[0])


	while len(negSamples) < numSamples:
		powerSequence = [0]*window_size
		act = posSamples[randint(0,len(posSamples)-1)]
		
		#As amotras positivas que não tem consumo (sum(x) = 0) não devem ser tratadas aqui, 
		#pois irá gerar amostras iguais mas com a classificação negativa, e isso pode prejudicar o treinamento.
		if sum(act) < 0.001:
			continue
		'''
		Cada amostra tem:
		60% de chance de ter de 2 a (1/20*(len(powerSequence)) de picos de energia aleatorios
		20% de chance de ter 1 pico aleatorio
		20% de chance de ser a permutação da assinatura
		'''
		r = randint(0,100)
		if r > 100:
			qtPicos = randint(2,int(len(powerSequence)/20))
			for j in range(qtPicos):
				powerSequence[randint(0,len(powerSequence)-1)] = act[randint(0,len(act)-1)]*0.5 if randint(0,1) == 0 else act[randint(0,len(act)-1)]
		elif r > 100:
			powerSequence[randint(0,len(powerSequence)-1)] = act[randint(0,len(act)-1)]*0.5 if randint(0,1) == 0 else act[randint(0,len(act)-1)]
		elif r > 40:
			fact = randint(10,25)/100
			#plt.plot(act)
			powerSequence = [x*fact for x in act]
			#plt.plot(powerSequence)
			#plt.show()
		else:
			#stam = randint(0,int(len(powerSequence))/2)
			#for j in range(stam):
			#	powerSequence[j] = random()
			a = copy.copy(act)
			shuffle(a)
			powerSequence = a
			#qtPicos = randint(2,int(len(powerSequence)/20))
			#for j in range(qtPicos):
			#	powerSequence[randint(0,len(powerSequence)-1)] = 
		


		negSamples.append(powerSequence)
		#y.append([0]*len(powerSequence))
		y.append(0)

	
	#Run wavelet transform
	for i in range(len(negSamples)):
		negSamples[i] = ds.reduce_data(negSamples[i],times = 2)

	#Run wavelet transform invert
	for i in range(len(negSamples)):
		negSamples[i] = ds.invert_dwt(negSamples[i],times=2)
	

	return negSamples,y

def get_negativeSamples_Weicong(posSamples,numSamples):
	negSamples = []
	y = []
	window_size = len(posSamples[0])


	for i in range(numSamples):
		powerSequence = [0]*window_size
		act = posSamples[randint(0,numSamples-1)]
		
		r = randint(0,100)
		if r > 50:
			powerSequence[randint(0,window_size-1)] = act[randint(0,window_size-1)]			
		else:
			shuffle(powerSequence) #Permutando 0's?
			
		negSamples.append(powerSequence)
		#y.append([0]*len(powerSequence))
		y.append(0)
	return negSamples,y


def get_samples(numSamples,appliance):
	
	x_train,y_train = get_positiveSamples(numSamples,appliance)
	
	x_train,y_train = list(x_train),list(y_train)
	
	x,y = get_negativeSamples(posSamples=x_train,numSamples=len(x_train))
	
	print(np.shape(x_train))
	
	#x_train,y_train = x,y	
	
	x_train = list(x_train)
	y_train = list(y_train)

	x_train.extend(x)
	y_train.extend(y)

	c = list(zip(x_train,y_train))
	shuffle(c)
	x_train,y_train = zip(*c)
	

	y_train = np.array(y_train).reshape(-1, 1)
	
	print(np.shape(x_train))
	print(np.shape(y_train))
	
	x_train = normalize(x_train, axis=0)
	#y_train = normalize(y_train, axis=0)

	'''
	plt.subplot(331)
	plt.title(y_train[0])
	plt.plot(x_train[0])
	plt.subplot(332)
	plt.title(y_train[1])
	plt.plot(x_train[1])
	plt.subplot(333)
	plt.title(y_train[2])
	plt.plot(x_train[2])
	plt.subplot(334)
	plt.title(y_train[3])
	plt.plot(x_train[3])
	plt.subplot(335)
	plt.title(y_train[4])
	plt.plot(x_train[4])
	plt.subplot(336)
	plt.title(y_train[5])
	plt.plot(x_train[5])
	plt.subplot(337)
	plt.title(y_train[6])
	plt.plot(x_train[6])
	plt.subplot(338)
	plt.title(y_train[7])
	plt.plot(x_train[7])
	plt.subplot(339)
	plt.title(y_train[8])
	plt.plot(x_train[8])
	plt.show()
	exit()
	'''

	x_train = np.array(x_train,dtype=float).reshape(np.shape(x_train)[0],np.shape(x_train)[1],1)
	y_train = np.array(y_train,dtype=float)

	return x_train,y_train
	

def train(appliance,numSamples):
	x_train,y_train = get_samples(numSamples,appliance)

	print("x_train shape:", np.shape(x_train))
	print("y_train shape:", np.shape(y_train))
	
	#print(x_train[0])
	#print(y_train[0])
	
	model = nn.build_model_PostProcessing(inputSize=len(x_train[0]),arch=3)
	print(model.summary())
	#exit()
	
	model.compile(loss='binary_crossentropy',optimizer='adam',metrics=['acc'])
	es = EarlyStopping(monitor='val_accuracy', mode='max', verbose=2,patience=20)

	
	history = model.fit(x_train, y_train,verbose=1,batch_size=128,epochs=300,validation_split=0.2,callbacks=[es])
	model.save('models/my_postProcessing_'+appliance+'.h5')
	exit()
	
	plt.subplot(211)
	plt.plot(history.history['accuracy'])
	plt.plot(history.history['val_accuracy'])
	plt.title('model accuracy')
	plt.ylabel('accuracy')
	plt.xlabel('epoch')
	plt.legend(['train', 'test'], loc='upper left')
	plt.subplot(212)
	# summarize history for loss
	plt.plot(history.history['loss'])
	plt.plot(history.history['val_loss'])
	plt.title('model loss')
	plt.ylabel('loss')
	plt.xlabel('epoch')
	plt.legend(['train', 'test'], loc='upper left')
	plt.show()

def my_normalization(timeserie):
	
	maxValue = max(timeserie)
	minValue = min(timeserie)

	for i in range(len(timeserie)):
		
		timeserie[i] = (timeserie[i]-minValue)/(maxValue-minValue)
	
	return timeserie,maxValue,minValue


def my_desnormalization(timeserie,maxValue,minValue):
	
	for i in range(len(timeserie)):
		
		timeserie[i] = timeserie[i]*(maxValue-minValue)+minValue
	
	return timeserie

def use(timeSerie,appliance):

	model = load_model('models/my_postProcessing_'+appliance+'.h5')
	tam = len(timeSerie)
	print('t',tam)
	point = 0
	inputSize = int(model.inputs[0].shape[1])
	
	timeSerie,maxValue,minValue = my_normalization(timeSerie)
	
	filtered = []
	count_true,count_false,count_zero = 0,0,0
	while point+inputSize < tam:
		
		if sum(timeSerie[point:point+inputSize]) < 0.001:
			count_zero +=1
		a = np.array(timeSerie[point:point+inputSize]).reshape(-1,1)
		a = np.expand_dims(a, axis=0)
		b = model.predict(a,batch_size=1)
		#print(b)
		if b > 0.5:
			count_true+=1
			#print("True")
			filtered.extend(timeSerie[point:point+inputSize])
		else:
			count_false+=1
			#print("False")
			filtered.extend([0]*inputSize)

		#if sum(timeSerie[point:point+inputSize])/inputSize > 0.3:
		#	#plt.plot(timeSerie[point:point+inputSize],label='sample')
		#	#plt.legend()
		#	#plt.show()

		##plt.show()
		point+=inputSize
	
	print('False:',count_false,'True:',count_true,'Zeros:',count_zero)
	#plt.plot(filtered,'--',label='filtrado')
	#plt.legend()
	#plt.show()
	return my_desnormalization(filtered,maxValue,minValue)



if __name__ == '__main__':
	train(appliance='dish washer',numSamples=1000)






